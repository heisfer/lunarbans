<?php
//require $root.'classes/game/cs16/Rcon.class.php';
session_start();
//preEcho($_SESSION);

$actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$requert = str_replace('/admin/','',$_SERVER['REQUEST_URI']);
$adminurl = $siteurl.$admindir.$requert;
if (isset($_SESSION['error']) && !empty($_SESSION['error']['alert'])) {
    if ($_SESSION['error']['success']) {
        ?>
        <p class="toastr toastr-success" style="display:none"><?= $_SESSION['error']['alert']; ?></p>
        <?php
        unset($_SESSION['error']);
    } else {
        ?>
        <p class="toastr toastr-error" style="display:none"><?= $_SESSION['error']['alert']; ?></p>
        <?php
        unset($_SESSION['error']);
    }
}
if($adminurl == url()) {
    if (!isset($_SESSION['lang'])) {
        require $rootadmin.'inc/lang/est.php';
    } else {
        $lang = $_SESSION['lang'];
        require $rootadmin."inc/lang/$lang.php";
    }
    if (!isset($_SESSION['acp'])) {
        header("Location: https://lb.lunar.icu");
    }
    $ScriptKaust = "/admin/";

    #eemaldame kausta osa URL reast
    $request = str_replace($ScriptKaust, "/", $_SERVER['REQUEST_URI']);
    #eemaldame $request eest / ehk /default_seo/ -> default_seo/
    $request = substr($request, 1, strlen($request));
    #teeme selle slashist (/) osadeks
    $req = explode('/', $request);
    #$kl->naita($_SESSION);


    if (@$_SESSION['acp']['priv']["can_$req[0]"] != 0 or $req[0] == 'index') {
        require $root.$admindir.'/inc/theme/dark/header.php';
        require $root.$admindir.'/inc/theme/dark/navbar.php';
        if(!empty($req[0]) and $req[0] != 'index') {

            #seega req[0] on failinimi ilma php-ta
            $file = $req[0].'.php';
            $file = $root.$admindir."/inc/theme/dark/$file";
            if(file_exists($file) and is_file($file)) {
                #failö on, seega laeme
                require_once($file);
            } else {
                #faili ei leitud. öae emda error või näita infi
                    ?>
                    <p>Faili <strong><?php echo $file; ?></strong> ei leitud raisk</p>
                    <?php
            }
        } else {
            #see on siis kavaaleht

            include $root.$admindir.'/inc/theme/dark/index.php';
        }
    } else {
        $_SESSION['error']['alert'] = "sul pole sellele lehele ligipääsu!";
        $_SESSION['error']['success'] = false;
        header("Location: https://lb.lunar.icu/admin/index");
    }



    ##require 'inc/theme/dark/index.php';

    require $root.$admindir.'/inc/theme/dark/footer.php';
} else {
    $list = allStatus();
    $privs = listOfGroups();
    $servers = array();
    $serverlist = array();
    $adminlist = array();
    $banlist = array();
    foreach ($list as $key => $value) {
        $name = explode("| ", $value['info']['host']);
        $servers[] = $name[1];
        $serverlist[] = $value['info'];
        $adminlist[$name[1]] = $value['adminlist'];
        if (isset($value['banlist'])) {
            $banlist[$name[1]] = @$value['banlist'];
        }

    }

    $title = "LunarBans";

        $ScriptKaust = "";
        require $root.'/template/header.php';
        #eemaldame kausta osa URL reast
        $request = str_replace($ScriptKaust, "/", $_SERVER['REQUEST_URI']);
        #eemaldame $request eest / ehk /default_seo/ -> default_seo/
        $request = substr($request, 1, strlen($request));
        #teeme selle slashist (/) osadeks
        $req = explode('/', $request);
        #$kl->naita($_SESSION);

        if(!empty($req[0]) and $req[0] != 'index') {
            #seega req[0] on failinimi ilma php-ta
            $file = $req[0];
            $file = $root."$file.php";
            if(file_exists($file) and is_file($file)) {
                #failö on, seega laeme
                require_once($file);
            } else {
                #faili ei leitud. öae emda error või näita infi
                ?>
                <p>Faili <strong><?php echo $file; ?></strong> ei leitud raisk</p>
                <?php
            }
        } else {
            #see on siis kavaaleht
            include $root.'/adminlist.php';
        }
        require $root.'/template/footer.php';

        ##require 'inc/theme/dark/index.php';

/*        require $root.$admindir.'/inc/theme/dark/footer.php';
    require 'template/header.php';
    require 'adminlist.php';
    require 'template/footer.php';*/
}

?>
