<?php
session_start();
if (isset($_SESSION['loggedIn'])) {
    header("Location: index.php");
}
require 'inc/db.inc.php';
require 'inc/permissions.inc.php';
require 'inc/functions.inc.php';



if (isset($_POST['username']) and isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $userAdmin = checkAdmin($username, $password);
    if ($userAdmin) {
        $sql = "SELECT * FROM amx_webadmins WHERE username='$username'";
        foreach ($db->query($sql) as $webAdmin) {
            $_SESSION["uid"]= $webAdmin['id'];
            $_SESSION["uname"]= $username;
            $_SESSION["email"]= $webAdmin['email'];
            $_SESSION["sid"]=session_id();
            $_SESSION['admin'] = true;
            $currentTime = time();
            $db->query("UPDATE amx_webadmins SET last_action='$currentTime' WHERE username='$username'");
        }
    }
    $userUser = checkUser($username, $password);
    if ($userUser) {
        $_SESSION['user'] = true;
        $_SESSION['userName'] = $username;
    }
}
