<?php

session_start();
require 'inc/functions.php';
#seo jaoks
$ScriptKaust = "/admin/";
require 'inc/theme/dark/header.php';
#eemaldame kausta osa URL reast
$request = str_replace($ScriptKaust, "/", $_SERVER['REQUEST_URI']);
#eemaldame $request eest / ehk /default_seo/ -> default_seo/
$request = substr($request, 1, strlen($request));
#teeme selle slashist (/) osadeks
$req = explode('/', $request);
#$kl->naita($_SESSION);

require 'inc/theme/dark/navbar.php';

if(!empty($req[0]) and $req[0] != 'index') {
    #seega req[0] on failinimi ilma php-ta
    $file = $req[0].'.php';
    $file = "inc/theme/dark/$file";
    if(file_exists($file) and is_file($file)) {
        #failö on, seega laeme
        require_once($file);
    } else {
        #faili ei leitud. öae emda error või näita infi
        ?>
        <p>Faili <strong><?php echo $file; ?></strong> ei leitud raisk</p>
        <?php
    }
} else {
    #see on siis kavaaleht
    include 'inc/theme/dark/index.php';
}

##require 'inc/theme/dark/index.php';

require 'inc/theme/dark/footer.php';
