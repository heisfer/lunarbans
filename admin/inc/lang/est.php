<?php
//permissions
$lang = [
    'permissions' => [
        'can_addadmin' => 'admini lisamine',
        'can_addsms' => 'SMSi lisamine',
        'can_admincfg' => 'Admini seaded',
        'can_adminlist' => 'Serveri admini nimekiri',
        'can_amxbans' => 'Amxbansi loomine ja muutmine',
        'can_banlist' => 'Bannide nimekiri',
        'can_privileges' => 'KP privileegide loomine"',
        'can_profile' => 'Enda profiilile ligipääs',
        'can_smslist' => 'SMS teenuste nimekiri',
        'can_smslog' => 'SMS logid',
        'can_log' => 'LunarBansi logid',
        'can_smstest' => 'SMS teenuse testimine',
        'can_users' => 'Kasutaja loomine ja muutmine',
    ],
];
?>
