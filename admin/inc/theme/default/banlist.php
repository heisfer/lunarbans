
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Ban list</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
$(document).ready(function(){
$('form').each(function() {
this.reset()
});
});
</script>
<script>
	function toggleMenu(id) {
		var e = document.getElementById(id);
		$(e).fadeToggle("fast");
	}
</script>
<div class="clickable">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Kuupäev</th>
                <th>Mängija</th>
                <th>Admin</th>
                <th>Server</th>
                <th>Põhjus</th>
                <th>Ketiv kuni</th>
                <th>Kehtiv</th>
            </tr>
        </thead>
        <tbody>
            <?= result_to_html($db); ?>
        </tbody>
    </table>
    <div class="clickable" style=" max-width: 400px; margin: 0 auto;">
        <table  class="table table-striped table-bordered table-hover">
            <tr class='greyrow'>
                <th colspan="2"  >Andmed</th>
            </tr>
            <tr>
                <td width="10%">Ban ID: </td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Mängija</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">IP:</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Admin:</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Kehtiv alates:</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Kehtiv kuni:</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Staatus:</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Server:</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Kaart:</td>
                <td></td>
            </tr>
            <tr>
                <td width="10%">Bannitud varasemalt:</td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
