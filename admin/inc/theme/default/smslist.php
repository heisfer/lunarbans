<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">SMS teenusde nimekiri</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
    <div class="panel panel-default">

    <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Teenuse nimi</th>
        <th>Teenuse kirjeldus</th>
        <th>Teenuse sisestus</th>
        <th>Teenuse kestvus</th>
        <th>Teenuse tasu</th>
    </tr>
    </thead>
    </table>
    </div>
    <!-- /.table-responsive -->

    <!-- /.panel-body -->

<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
