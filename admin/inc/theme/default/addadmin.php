<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">Admini lisamine</h1>
      </div>
      <!-- /.col-lg-12 -->

      <div class="col-xs-6">
          <form class="" action="addadmin.php" method="post">
              <div class="form-group">
              <label>Username</label>
              <input name="username" class="form-control" required>
              </div>
              <div class="form-group">
              <label>Password</label>
              <input name="password" type="password" class="form-control" required>
              </div>
              <div class="form-group">
              <label>SteamID/IP/name</label>
              <input name="usertype" class="form-control" required>
              </div>
              <div class="form-group">
                  <label>Õigus</label>
                  <select name="access" class="form-control">

                    <?php $arrAdminGroups = listOfGroups();
                    foreach ($arrAdminGroups as $key => $val) : ?>
                        <option value="<?php echo $val['permissions']; ?>"><?php echo $val['rank']; ?></option>
                    <?php endforeach; ?>
                  </select>
              </div>
              <div class="form-group">
                  <label>Millele admin läheb?</label>
                  <select name="flags" class="form-control">
                      <option value="a">name</option>
                      <option value="ad">IP</option>
                      <option value="ac">SteamID</option>
                  </select>
              </div>
              <div class="form-group">
                  <label>Kestvus <small>(päevades)</small> </label>
                  <input name="days" class="form-control">
              </div>
              <div class="form-group">
                  <label>Vali server</label>
                  <select multiple name="server[]" class="form-control" required>
                    <?php foreach ($arrServer as $serverList) : ?>
                        <?= $serverList ?>
                    <?php endforeach; ?>
                  </select>
              </div>
              <div class="form-group">
                  <label>staatilise ban-ajaga:</label>
                  <select name="staticBan" class="form-control">
                      <option value="yes">Jah</option>
                      <option value="no">Ei</option>
                  </select>
              </div>
              <button type="submit" name="submit" class="btn btn-default">Loo Admin</button>
              <button type="reset" class="btn btn-default">Tee puhtaks</button>
              <br><br><br><br><br><br>
          </form>
      </div>
      <div class="col-xs-6">

      </div>
  </div>
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
