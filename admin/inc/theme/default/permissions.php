<style>
.target {
display: block;
}

.target.hidden {
display: none;
}
</style>
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">KP Õigused</h1>
      </div>
      <!-- /.col-lg-12 -->
      <h1>Tulemas! LunarBans v2</h1>
  </div>
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script src='https://static.codepen.io/assets/common/stopExecutionOnTimeout-de7e2ef6bfefd24b79a3f68b414b87b8db5b08439cac3f1012092b2290c719cd.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
var filter = document.getElementById('filter');

filter.addEventListener('change', function () {
  var option = this.options[this.selectedIndex];
  var targets = option.dataset.targets.split(/(\s+)/);
  for (var target of document.getElementsByClassName('target')) {
    if (targets.indexOf(target.id) >= 0)
    target.classList.remove('hidden');else

    target.classList.add('hidden');
  }
});
//# sourceURL=pen.js
</script>
