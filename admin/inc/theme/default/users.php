<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">KP Kasutajad</h1>
      </div>

      <div class="col-xs-6">
          <form class="" action="users.php" method="post">
              <div class="form-group">
                  <label>Kasutaja nimi</label>
                  <input name="uadmin"  class="form-control" required>
              </div>
              <div class="form-group">
                  <label>Parool</label>
                  <input name="padmin" value="" type="password" class="form-control" required>
              </div>
              <div class="form-group">
                  <label>Email</label>
                  <input name="eadmin" type="email" class="form-control" required>
              </div>
              <div class="form-group">
                  <label>Admin Level</label>
                  <select name="level" class="form-control">
                      <?= getLevels($db); ?>
                  </select>
              </div>
              <button name="submit" type="submit" class="btn btn-default">Loo Kasutaja</button>
              <button type="reset" class="btn btn-default">Tühjenda</button>
          </form>
      </div>

      <div class="col-xs-6">
          <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
              <th>#</th>
              <th>Kasutaja</th>
              <th>Level</th>
              <th>E-mail</th>
              <th>Viimati käis</th>
              <th width="10%">Kustuda</th>
          </tr>
          </thead>
          <tbody>
              <?= getWebAdmins($db);  ?>
          </tbody>
          </table>
          </div>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
