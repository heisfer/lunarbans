<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Admin List</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
    <table class="table">
        <thead>
            <tr>
                <th>Kasutajanimi</th>
                <th>Õigused</th>
                <th>Kehtiv alates</th>
                <th>Kehtiv Kuni</th>
                <th>Kehtiv</th>
            </tr>
        </thead>
        <tbody>
            <?= adminList(); ?>
        </tbody>
    </table>

<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
