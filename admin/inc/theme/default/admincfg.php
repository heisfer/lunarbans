<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Admini õigused</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php if (!empty($errMsg)) : ?>
    <div class="alert alert-danger">
    <?= $errMsg ?>
    </div>
<?php elseif (!empty($sucess)) : ?>
    <div class="alert alert-success">
    <?= $sucess ?>
    </div>
<?php endif; ?>
<div class="col-xs-6">
    <h3>Loo õigused</h3>
<form class="" action="admincfg.php" method="post">
    <div class="form-group">
    <label>Ranki nimi</label>
    <input name="group" class="form-control">
    </div>
    <div class="form-group">
    <label>Õigused</label>
    <input name="permissions" class="form-control">
    <button type="submit" name="submit" class="btn btn-default">Loo group</button>
    <button type="reset" class="btn btn-default">tühenda</button>
    </div>
</form>
<hr>
<h4>Õigused</h4>
<p>
a - Immunity (cant be kicked / banned etc.) <br>
b - Reserved Slots (can use reserved Slots) <br>
c - amx_kick Command <br>
d - amx_ban and amx_unban Command <br>
e - amx_slay and amx_slap Command <br>
f - amx_map Command <br>
g - amx_cvar Command (not all CVARS available) <br>
h - amx_cfg Command <br>
i - amx_chat and other Chat-Commands <br>
j - amx_vote and other Vote-Commands <br>
k - Access to sv_password cvar (through amx_cvar Command) <br>
l - Access to amx_rcon command and rcon_password cvar (through amx_cvar Command) <br>
m - Userdefined Level A (for other Plugins) <br>
n - Userdefined Level B <br>
o - Userdefined Level C <br>
p - Userdefined Level D <br>
q - Userdefined Level E <br>
r - Userdefined Level F <br>
s - Userdefined Level G <br>
t - Userdefined Level H <br>
u - Menu-Access <br>
z - User (no Admin)</p>
</div>
<div class="col-xs-6">
    <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Rank</th>
        <th>Õigused</th>
        <th width="10%">Kusduta</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $list = lb_adminPrivileges();
        if ($list != null) {
            $i = 0;
            foreach ($list as $key => $value) {
                ?>
                <tr id="<?php echo $value['id']; ?>">
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $value['rank']; ?></td>
                    <td><?php echo $value['permissions']; ?></td>
                    <td></td>
                </tr>

                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="3" style="color:red">Lista Admini õigus</td>
            </tr>
            <?php
        }
         ?>
    </tbody>
    </table>
    </div>
</div>

    <!-- /.table-responsive -->

    <!-- /.panel-body -->

<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
