<div class="page-content">
  <div class="page-header">
    <div class="container-fluid">
      <h2 class="h5 no-margin-bottom">Admini Lisamine</h2>
    </div>
  </div>
  <section class="no-padding-top no-padding-bottom">
      <?php $arrAdminGroups = listOfGroups();
      if($arrAdminGroups != null):?>
      <div class="col-xs-6">
          <form class="" action="process.php" method="post">
              <div class="form-group">
              <label>Username</label>
              <input name="username" class="form-control" required>
              </div>
              <div class="form-group">
              <label>Password</label>
              <input name="password" type="password" class="form-control" required>
              </div>
              <div class="form-group">
              <label>SteamID/IP/name</label>
              <input name="usertype" class="form-control" required>
              </div>
              <div class="form-group">
                  <label>Õigus</label>
                  <select name="access" class="form-control">
                      <?php
                      foreach ($arrAdminGroups as $key => $val) : ?>
                      <option value="<?php echo $val['permissions']; ?>"><?php echo $val['rank']; ?></option>
                  <?php endforeach; ?>
                  </select>
              </div>
              <div class="form-group">
                  <label>Millele admin läheb?</label>
                  <select name="flags" class="form-control">
                      <option value="a">name</option>
                      <option value="ad">IP</option>
                      <option value="ac">SteamID</option>
                  </select>
              </div>
              <div class="form-group">
                  <label>Kestvus <small>(päevades)</small> </label>
                  <input name="days" class="form-control">
              </div>
              <div class="form-group">
                  <label>Vali server</label>
                  <select multiple name="server[]" class="form-control" id="selected" required>
                    <?php
                    $arrServer = serverList();
                    foreach ($arrServer as $keys => $values) : ?>

                        <?php foreach ($values as $key => $val): ?>
                            <option value='{"db": "<?php echo($keys); ?>", "id": "<?php echo $val['id'] ?>"}'> <?php echo $val['hostname']; ?> </option>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                  </select>
              </div>
              <div class="form-group">
                  <label>staatilise ban-ajaga:</label>
                  <select name="staticBan" class="form-control">
                      <option value="yes">Jah</option>
                      <option value="no">Ei</option>
                  </select>
              </div>
              <input type="submit" id="submit" name="addAdmin" value="Loo Admin" class="btn btn-primary">
              <button type="submit" name="submit" class="btn btn-default"></button>
              <button type="reset" class="btn btn-default">Tee puhtaks</button>
              <br><br><br><br><br><br>
          </form>
      </div>
  <?php else:
       ?> <script>window.location="admincfg";</script>
  <?php endif; ?>
  </section>
