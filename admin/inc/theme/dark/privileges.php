<div class="page-content">
    <div class="page-header no-margin-bottom">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Privileegid</h2>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">Avaleht</a></li>
            <li class="breadcrumb-item active">Privileegid</li>
        </ul>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="row">
            <div class="col-lg-6">
                <div class="block">
                    <div class="title"><strong class="d-block">Loo privileegi tüüp</strong></div>
                    <div class="block-body">
                        <form action="process.php" method="post">

                            <div class="form-group">
                                <label class="form-control-label">Privileegi tüüp</label>
                                <input type="text" name="privName" placeholder="Privileegi tüüp" class="form-control">
                            </div>
                            <?php $privileges = privileges();
                            foreach ($privileges[0] as $key => $value) {
                                if ($key != "name" && $key != "id") {
                                    ?>
                                    <div class="i-checks">
                                        <input type="hidden" name="<?php echo $key; ?>" value="0" />
                                        <input id="dashboard" type="checkbox" name="<?php echo $key; ?>" value="1" class="checkbox-template">
                                        <label for="dashboard"><?php echo $lang['permissions'][$key]; ?></label>
                                    </div>
                                    <?php
                                }
                            }?>
                            <!--<div class="i-checks">
                                <input type="hidden" name="privilege[0]" value="0" />
                                <input id="dashboard" type="checkbox" name="privilege[0]" value="1" class="checkbox-template">
                                <label for="dashboard">Avaleht</label>
                            </div>
                            <div  class="i-checks">
                                <input type="hidden" name="privilege[1]" value="0" />
                                <input id="amxbans" type="checkbox" name="privilege[1]" value="1" class="checkbox-template">
                                <label for="amxbans">Amxbans haldus</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[2]" value="0" />
                                <input id="server" type="checkbox" name="privilege[2]" value="1" class="checkbox-template">
                                <label for="server">Serveri haldus</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[3]" value="0" />
                                <input id="banlist" type="checkbox" name="privilege[3]" value="1" class="checkbox-template">
                                <label for="banlist">Bännide nimiekiri</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[4]" value="0" />
                                <input id="addban" type="checkbox" name="privilege[4]" value="1" class="checkbox-template">
                                <label for="addban">Bänni lisamine</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[5]" value="0" />
                                <input id="banreasons" type="checkbox" name="privilege[5]" value="1" class="checkbox-template">
                                <label for="banreasons">Bänni põhjused</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[6]" value="0" />
                                <input id="adminlist" type="checkbox" name="privilege[6]" value="1" class="checkbox-template">
                                <label for="adminlist">Admini nimekiri</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[7]" value="0" />
                                <input id="addadmin" type="checkbox" name="privilege[7]" value="1" class="checkbox-template">
                                <label for="addadmin">Admini lisamine</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[8]" value="0" />
                                <input id="adminprivilegs" type="checkbox" name="privilege[8]" value="1" class="checkbox-template">
                                <label for="adminprivilegs">Admini õigused</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[9]" value="0" />
                                <input id="sms" type="checkbox" name="privilege[9]" value="1" class="checkbox-template">
                                <label for="sms">SMS teenused</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[10]" value="0" />
                                <input id="addsms" type="checkbox" name="privilege[10]" value="1" class="checkbox-template">
                                <label for="addsms">SMS lisamine</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[11]" value="0" />
                                <input id="users" type="checkbox" name="privilege[11]" value="1" class="checkbox-template">
                                <label for="users">KP kasutajad</label>
                            </div>
                            <div class="i-checks">
                                <input type="hidden" name="privilege[12]" value="0" />
                                <input id="privileges" type="checkbox" name="privilege[12]" value="1" class="checkbox-template">
                                <label for="privileges">KP privileegid</label>
                            </div>-->
                            <div class="form-group">
                                <input type="submit" value="Loo privileeg" name="addPrivilege" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block margin-bottom-sm">
                    <div class="title"><strong>Privileegid</strong></div>
                    <div class="table-responsive">
                        <table class="table table-hover ">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Privileegi tüüp</th>
                                    <th>Muuda/kustuda</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach (privileges() as $key => $val): ?>
                                    <tr>
                                        <td class="id"><?php echo $val["id"] ?></td>
                                        <td class="privilege"><?php echo $val["name"]; ?></td>
                                        <td class="text-center"><i class="fas fa-edit edit"  style="color: green; cursor: pointer;"></i>    <i class="fas fa-trash-alt delete"  style="color: #96281b; cursor: pointer;"></i></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="popWarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title" id="exampleModalLabel" style="color:red; text-align: center;"> OLED KINDEL?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="tolvan"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Ei</button>
                    <form class="" action="process.php" method="post">
                        <input type="hidden" name="delID" class="delID" value="">
                        <input type="submit"  class="btn btn-danger" name="delPriv" value="Jah">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="popEdit" tabindex="-1" role="dialog" aria-labelledby="privilegesUpdate" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title" id="privilegesUpdate" style="text-align: center;"> Privileegi uuendus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form  action="process.php" method="post">
                        <p id="editForm"><br></p>
                        <button type="button" class="btn btn-success" data-dismiss="modal">Sulge</button>
                        <input type="hidden" name="editID" class="editID" value="">
                        <input type="submit"  class="btn btn-danger" name="editPriv" value="Uuenda privileeg">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<script>
$(document).ready(function(){
    $(".table").simplePagination({
        perPage: 7,
        previousButtonClass: 'btn btn-light',
        nextButtonClass: 'btn btn-light',
    });

});
$(".delete").click(function() {
    var id = $(this).closest("tr").find(".id").text();
    var privilege = $(this).closest("tr").find(".privilege").text();
    $('#popWarning').modal('show');
    $(".delID").val(id);
    var priv = ["Avaleht: ", "Amxbans haldus: ", "Serveri haldus: ", "Bännide nimiekiri: ", "Bänni lisamine: ", "Bänni põhjused: ", "Admini nimekiri: ", "Admini lisamine: ", "Admini õigused: ", "SMS teenused: ", "SMS lisamine: ", "KP kasutajad: ", "KP privileegid: "]
    $.ajax({
        type: 'POST',
        url: 'process.php',
        data: { getPrivileges: id},
        dataType: 'json',
        success: function(response) {
            var text = "Privileegi nimi: " + privilege + "<br>";
            var i;2
            $.each(response, function(i, item) {
                if (item == 1) {
                    item = "<i class=\"fas fa-check\"  style=\"color: green\"></i>";
                } else {
                    item = "<i class=\"fas fa-times\"  style=\"color: red\"></i>";
                }
                text += priv[i] + item + "<br>";
            });
            document.getElementById("tolvan").innerHTML = text;
            console.log(text);
        }
    });
});
$(".edit").click(function() {
    var id = $(this).closest("tr").find(".id").text();
    var privilege = $(this).closest("tr").find(".privilege").text();
    $('#popEdit').modal('show');
    $(".editID").val(id);
    var priv = ["Avaleht: ", "Amxbans haldus: ", "Serveri haldus: ", "Bännide nimiekiri: ", "Bänni lisamine: ", "Bänni põhjused: ", "Admini nimekiri: ", "Admini lisamine: ", "Admini õigused: ", "SMS teenused: ", "SMS lisamine: ", "KP kasutajad: ", "KP privileegid: "]
    $.ajax({
        type: 'POST',
        url: 'process.php',
        data: { getPrivileges: id},
        dataType: 'json',
        success: function(response) {
            var text = "<div class=\"form-group\">";
            text += "<label class=\"form-control-label\">Privileegi tüüp</label>";
            text += "<input type=\"text\" name=\"privName\"  value=" + privilege + " placeholder=\"Privileegi tüüp\"  class=\"form-control\">";
            text += "</div>";
            var count = 0;
            $.each(response, function(i, item) {
                if (item == 1) {
                    item = "checked";
                } else {
                    item = "";
                }
                text += "<div class=\"i-checks\">";
                text += "<input type=\"hidden\" name=\"privilege[" + count + "]\" value=\"0\" />";
                text += "<input type=\"checkbox\" name=\"privilege[" + count + "]\" value=\"1\" class=\"checkbox-template\" " + item + ">";
                text += "<label> " + priv[i] + " </label>";
                text += "</div>"
                count++;
            });
            document.getElementById("editForm").innerHTML = text;
            console.log(text);
        }
    });
});
</script>
