<div class="page-content">
    <div class="page-header no-margin-bottom">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Amxbansi andmebaasid</h2>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">Avaleht</a></li>
            <li class="breadcrumb-item active">Amxbansi andmebaasid</li>
        </ul>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="row">
            <div class="col-lg-6">
                <div class="block">
                    <div class="title"><strong class="d-block">Loo andmebaas</strong></div>
                    <div class="block-body">
                        <form action="process.php" method="post">
                            <div class="form-group">
                                <label class="form-control-label">Serveri nimi</label>
                                <input type="text" name="serverName" placeholder="Serveri nimi" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Andmebaas</label>
                                <input type="text" name="serverDatabase" placeholder="Serveri Andmebaas" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="form-control-label">AmxBans Versioon</label>
                                <select class="form-control" name="version">
                                    <?php
                                    $versions = listOfAmxVersions();
                                    for ($i=0; $i < sizeof($versions); $i++) {
                                        ?>
                                        <option value="<?php echo "$versions[$i]"; ?>">Amxbans v<?php echo $versions[$i]; ?></option>
                                        <?php
                                    }

                                     ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="createAmxBans" value="Loo Andmebaas" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block margin-bottom-sm">
                    <div class="title"><strong>Andmebaasid</strong></div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Server</th>
                                    <th>Andmebaas</th>
                                    <th>Versioon</th>
                                    <th>Valik</th>
                                </tr>
                            </thead>
                            <?php $servers = listOfAmxServers(); ?>
                            <tbody>
                                <?php
                                if ($servers) {
                                    $i = 1;
                                    foreach ($servers as $key => $value) {
                                        ?><tr id="<?php echo $value['id']; ?>">
                                            <td id="nr"> <?php echo $i; ?></td>
                                            <td id="server"><?php echo $value['servername']; ?></td>
                                            <td id="amxbans"><?php echo $value['amxbans']; ?></td>
                                            <td id="version"><?php echo $value['version']; ?></td>
                                            <td class="text-center"> <form class="" action="process.php" method="post">
                                                <input type="hidden" name="amxdb" value="<?php echo $value['amxbans']; ?>">
                                                <input class="fas fa-download download" type="submit" name="" value="">
                                                <!--<i class="fas fa-download download"  style="color: 	#4C6EF5; cursor: pointer;"></i>-->
                                            </form>   <i class="fas fa-trash-alt delete"  style="color: #96281b; cursor: pointer;"></i></td>
                                        </tr>
                                                <?php
                                        $i++;
                                    }
                                }
 ?>
                            </tbody>
                        </table>
                        <div class="modal fade" id="deleteAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h5 class="modal-title" id="exampleModalLabel" style="color:red; text-align: center;"> OLED KINDEL?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p id="delete-info"></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" data-dismiss="modal">Ei</button>
                                        <form class="" action="process.php" method="post">
                                            <input type="hidden" name="delID" class="delID" value="">
                                            <input type="submit"  class="btn btn-danger" name="delDatabase" value="Jah">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.row -->
</div>
  <!-- /.container-fluid -->
</div>

<script>
$(".delete").click(function() {
    var id = $(this).closest('tr').attr('id');
    var servername = $(this).closest("tr").find("#server").text();
    var amxbans = $(this).closest("tr").find("#amxbans").text();
    var version = $(this).closest("tr").find("#version").text();
    $('#deleteAccount').modal('show');
    $(".delID").val(id);
    var text = "Serveri nimi: " + servername + "</br>";
    text += "Andmebaas: " + amxbans + "</br>";
    text += "Amxbans version: " + version;
    document.getElementById("delete-info").innerHTML = text;
});
$(".download").click(function() {
    var database =  $(this).closest("tr").find("#amxbans").text();
    $.ajax({
    url: "ajax/backup.php",
    type: "post",
    data: { database: database},
    success: function(recd){
        alert(recd);
        $('#btnMysqlBkup').hide();
    }
});
});
</script>
