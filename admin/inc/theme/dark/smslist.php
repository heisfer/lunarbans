<div class="page-content">
  <div class="page-header">
    <div class="container-fluid">
      <h2 class="h5 no-margin-bottom">SMS nimekiri</h2>
    </div>
  </div>
  <section class="no-padding-top no-padding-bottom">
<?php
$sms = listOfSMSServices();
$privs = listOfGroups();
$access = "";
 ?>
    <div class="table-responsive table-hover">
               <table class="table">
                 <thead>
                   <tr>
                       <th>Nimi</th>
                       <th>Number</th>
                       <th>Sisu</th>
                       <th>Summa</th>
                       <th>Rank</th>
                       <th>Server</th>
                   </tr>
                 </thead>
                 <tbody>
                     <?php
                     foreach ($sms as $keys => $value) {
                             foreach ($privs as $kpriv => $priv) {
                                 if ($value['permission'] == $priv['permissions']) {
                                     $access = $priv['rank'];
                                 }
                             }
                      ?>
                      <tr>
                          <td><?php echo $value['serviceName']; ?></td>
                          <td><?php echo $value['number']; ?></td>
                          <td><?php echo $value['message']; ?></td>
                          <td><?php echo $value['price']."€"; ?></td>
                          <td><?php echo $access; ?></td>
                          <td>
                              <?php
                              $json = $value['server'];
                              $servers = json_decode($json, true);
                              foreach ($servers as $key => $value) {
                                  $serverid = $value['id'];
                                  $database = $value['db'];
                                  $server = serverList($database, $serverid);
                                  foreach ($server as $val) {
                                      echo $val['hostname'];
                                  }
                                  echo "<br>";
                              }?>
                          </td>
                      </tr>
                      <?php

              } ?>
                 </tbody>
               </table>
               <pre>
                    <?php // print_r(adminList()); ?>
               </pre>
             </div>

  </section>
