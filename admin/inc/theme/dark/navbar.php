<header class="header">
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid d-flex align-items-center justify-content-between">
            <div class="navbar-header">
                <!-- Navbar Header--><a href="index" class="navbar-brand">
                <div class="brand-text brand-big visible text-uppercase"><strong class="text-primary">Lunar</strong><strong>Bans</strong></div>
                <div class="brand-text brand-sm"><strong class="text-primary">L</strong><strong>B</strong></div></a>
                <!-- Sidebar Toggle Btn-->
                <button class="sidebar-toggle"><i class="fa fa-arrow-left"></i></button>
            </div>
            <div class="right-menu list-inline no-margin-bottom">
                <!-- Languages dropdown    -->
                <div class="list-inline-item dropdown"><a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link language dropdown-toggle"><img src="inc/theme/dark/img/flags/16/EE.png" alt="Eesti"><span class="d-none d-sm-inline-block">Eesti</span></a>
                    <div aria-labelledby="languages" class="dropdown-menu"><a rel="nofollow" href="#" class="dropdown-item"> <img src="inc/theme/dark/img/flags/16/US.png" alt="English" class="mr-2"><span>English</span></a></div>
                </div>
                <!-- Log out -->
                <div class="list-inline-item logout">                   <a id="logout" href="process.php" class="nav-link"> <span class="d-none d-sm-inline">Logi välja </span><i class="icon-logout"></i></a></div>
            </div>
        </div>
    </nav>
</header>
<div class="d-flex align-items-stretch">
    <!-- Sidebar Navigation-->
    <nav id="sidebar">
        <!-- Sidebar Header-->
        <div class="sidebar-header d-flex align-items-center">
            <a href="profile">
                <div class="avatar"><img src="inc/theme/dark/img/lumineo.jpeg" alt="..." class="img-fluid rounded-circle"></div>
                <div class="title">
                    <h1 class="h5">Lumineo</h1>
                    <p>Omanik</p>
                </div></a>
            </div>
            <!-- Sidebar Navidation Menus--><span class="heading">Peamine</span>
            <ul class="list-unstyled">
                <li class="nav-item"><a class="nav-link" href="index"> <i class="fa fa-home"></i>Avaleht </a></li>
                <li><a href="#serverManagment" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-server"></i>Serveri haldus </a>
                    <ul id="serverManagment" class="collapse list-unstyled ">
                        <li><a href="amxbans">Amxbans andmebaasid</a></li>
                    </ul>
                </li>
                <li><a href="#banManagment" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-ban"></i>Bänni haldus </a>
                    <ul id="banManagment" class="collapse list-unstyled ">
                        <li><a href="banlist">Bänni nimekiri</a></li>
                        <li><a href="addban">Bänni lisamine</a></li>
                        <li><a href="banreasons">Bänni põhjused</a></li>
                    </ul>
                </li>
                <li><a href="#adminManagment" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-user"></i>Admini haldus </a>
                    <ul id="adminManagment" class="collapse list-unstyled ">
                        <li><a href="adminlist">Admini nimekiri</a></li>
                        <li><a href="addadmin">Admini lisamine</a></li>
                        <li><a href="admincfg">Admini õigused</a></li>
                    </ul>
                </li>
                <li><a href="#serviceManagment" aria-expanded="false" data-toggle="collapse"> <i class="fas fa-sms"></i>SMS haldamine </a>
                    <ul id="serviceManagment" class="collapse list-unstyled ">
                        <li><a href="smslist">SMS teenused</a></li>
                        <li><a href="addsms">SMS lisamine</a></li>
                        <li><a href="smslog">SMS log</a></li>
                        <li><a href="smstest">SMS test</a></li>
                    </ul>
                </li>
                <li><a href="#userManagment" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-user-plus"></i>Kasutajate haldus </a>
                    <ul id="userManagment" class="collapse list-unstyled ">
                        <li><a href="users">KP kasutajad</a></li>
                        <li><a href="privileges">KP õigused</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- Sidebar Navigation end-->
