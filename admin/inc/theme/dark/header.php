<!DOCTYPE html>
<html lang="ee">
<head>
    <base href="<?php echo 'https://'.$_SERVER['HTTP_HOST'] . $ScriptKaust; ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LUNARBANS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="inc/theme/dark/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="inc/theme/dark/vendor/toastr/css/toastr.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <!-- Custom Font Icons CSS-->
    <link rel="stylesheet" href="inc/theme/dark/css/font.css">
    <!-- Google fonts - Muli-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="inc/theme/dark/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="inc/theme/dark/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="inc/theme/dark/img/favicon.ico">

    <script src="inc/theme/dark/vendor/jquery/jquery.min.js"></script>
    <script src="inc/theme/dark/vendor/jquery-simplePagination/jquery.simplePagination.js"></script>
    <script src="inc/theme/dark/vendor/toastr/js/toastr.min.js"></script>
</head>
<body>
