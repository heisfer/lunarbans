<div class="page-content">
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Admini õigused</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="row">
            <div class="col-lg-6">
                <div class="block">
                    <div class="title"><strong class="d-block">Loo õigused</strong></div>
                    <div class="block-body">
                        <form class="" action="process.php" method="post">
                            <div class="form-group">
                                <label class="form-control-label" data-toggle="tooltip" title"sa oled jobu">Ranki nimi</label>
                                <input type="text" name="rank" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Õigused</label>
                                <input type="text" name="permission" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="addGroup" value="Loo konto" class="btn btn-primary">
                                <button type="reset" class="btn btn-primary">Tühjenda</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block margin-bottom-sm">
                    <div class="title"><strong>Õiguste tabel</strong></div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Rank</th>
                                        <th>Õigused</th>
                                        <th>Kusduta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $list = listOfGroups();
                                    if ($list != null) {
                                        $i = 1;
                                        foreach ($list as $key => $value) {
                                            ?>
                                            <tr class="text-center" id="<?php echo $value['id']; ?>">
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $value['rank']; ?></td>
                                                <td><?php echo $value['permissions']; ?></td>
                                                <td class="text-center"><i class="fas fa-edit edit"  style="color: green; cursor: pointer;"></i>    <i class="fas fa-trash-alt delete"  style="color: #96281b; cursor: pointer;"></i></td>
                                            </tr>

                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="4" style="color:red; text-align: center;">Lista Admini õigus</td>
                                        </tr>
                                        <?php
                                    }
                                     ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="block">
                        <div class="title"><strong class="d-block">õigused</strong></div>
                            <div class="block-body">
                                <p>
                                a - Immunity (cant be kicked / banned etc.) <br>
                                b - Reserved Slots (can use reserved Slots) <br>
                                c - amx_kick Command <br>
                                d - amx_ban and amx_unban Command <br>
                                e - amx_slay and amx_slap Command <br>
                                f - amx_map Command <br>
                                g - amx_cvar Command (not all CVARS available) <br>
                                h - amx_cfg Command <br>
                                i - amx_chat and other Chat-Commands <br>
                                j - amx_vote and other Vote-Commands <br>
                                k - Access to sv_password cvar (through amx_cvar Command) <br>
                                l - Access to amx_rcon command and rcon_password cvar (through amx_cvar Command) <br>
                                m - Userdefined Level A (for other Plugins) <br>
                                n - Userdefined Level B <br>
                                o - Userdefined Level C <br>
                                p - Userdefined Level D <br>
                                q - Userdefined Level E <br>
                                r - Userdefined Level F <br>
                                s - Userdefined Level G <br>
                                t - Userdefined Level H <br>
                                u - Menu-Access <br>
                                z - User (no Admin)</p>
                            </div>
                    </div>
            </div>
        </section>
        <script>
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
        });
        </script>
