<div class="page-content">
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">SMS test</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="row">
            <div class="col-lg-6">
                <form action="smstest/" id="form">
                    <div class="form-group">
                        <label>Teenuse nimi</label>
                        <select class="form-control" name="service_id">
                            <?php $smslist = listOfSMSServices();
                            foreach ($smslist as $key => $value): ?>
                                <option value="<?php echo $value['service_id']; ?>"><?php echo $value['serviceName']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sõnumi sisu</label>
                        <input type="text" name="message" placeholder="Sõnumi sisu" class="form-control">
                    </div>
                    <button  id="shit" type="button" class="btn btn-primary">SMS test</button>
                </form>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Tulemus</label>
                    <textarea name="name" rows="8" class="form-control" cols="80"></textarea>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
    $('#shit').on('click', function() {

        var message = $('input[name=message]').val();
        var id = $('select[name=service_id]').val();
        var baseurl = window.location.origin;
        baseurl += "/sms.php?service_id=" + id + "&message=" + message + "&test=true";
        $.ajax
        ({
            url: baseurl,
            success: function(data)
            {
                $('textarea').val(data);
            }
        });
    });


    </script>
