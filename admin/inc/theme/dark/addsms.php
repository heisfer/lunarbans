<div class="page-content">
    <div class="page-header no-margin-bottom">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">SMS Lisamine</h2>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">Avaleht</a></li>
            <li class="breadcrumb-item active">SMS Lisamine</li>
        </ul>
    </div>
    <?php
    $privs = listOfGroups();
    ?>
    <section class="no-padding-top no-padding-bottom">
        <div class="container">
              <div class="block">
                <div class="title"><strong class="d-block">Andmed</strong><span class="d-block">Ole väga täpne andmete suhtes</span></div>
                <div class="block-body">
                  <form action="process.php" method="post">
                    <div class="form-group">
                      <label class="form-control-label">Teenuse Nimi</label>
                      <input type="text" name="serviceName" placeholder="Teenuse Nimi" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-control-label">Secret Key</label>
                      <input type="text" name="secretKey" placeholder="Secret Key" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-control-label">Number</label>
                      <input type="number" name="number" placeholder="Number" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-control-label">Sõnum</label>
                      <input type="text" name="message" placeholder="Sõnum" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="form-control-label">Summa</label>
                      <input type="text" name="price" placeholder="Summa" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Õigus</label>
                        <select class="form-control" name="permission">
                            <?php foreach ($privs as $key => $val): ?>
                                <option value="<?php echo $val['permissions']; ?>"> <?php echo $val['rank']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                  <div class="form-group">
                      <label>Vali server</label>
                      <select multiple name="server[]" class="form-control" id="selected" required>
                        <?php
                        $arrServer = serverList();
                        foreach ($arrServer as $keys => $values) : ?>

                            <?php foreach ($values as $key => $val): ?>
                                <option value='{"db": "<?php echo($keys); ?>", "id": "<?php echo $val['id'] ?>"}'> <?php echo $val['hostname']; ?> </option>
                                <option value='{"db": "lb_test", "id": "1"}'>aaa</option>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                      </select>
                  </div>
                    <div class="form-group">
                        <input type="hidden" name="addsms" value="addsms">
                      <input type="submit" value="Lisa sms teenus" class="btn btn-primary">
                    </div>
                  </form>
                </div>
              </div>
    </div>
    </section>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
