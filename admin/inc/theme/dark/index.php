      <div class="page-content">
        <div class="page-header">
          <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Avaleht</h2>
          </div>
        </div>
        <section class="no-padding-top no-padding-bottom">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="statistic-block block">
                  <div class="progress-details d-flex align-items-end justify-content-between">
                    <div class="title">
                      <div class="icon"><i class="fa fa-user"></i></div><strong>Adminid</strong>
                    </div>
                    <div class="number dashtext-1"><?php echo countAdmins(); ?></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="statistic-block block">
                  <div class="progress-details d-flex align-items-end justify-content-between">
                    <div class="title">
                      <div class="icon"><i class="fa fa-user-plus"></i></div><strong>KP Kasutajad</strong>
                    </div>
                    <div class="number dashtext-2"><?php echo countUsers(); ?></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="statistic-block block">
                  <div class="progress-details d-flex align-items-end justify-content-between">
                    <div class="title">
                      <div class="icon"><i class="fa fa-server"></i></div><strong>Serverid</strong>
                    </div>
                    <div class="number dashtext-3"><?php echo countServers(); ?></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="statistic-block block">
                  <div class="progress-details d-flex align-items-end justify-content-between">
                    <div class="title">
                      <div class="icon"><i class="fa fa-ban"></i></div><strong>Bännitud</strong>
                    </div>
                    <div class="number dashtext-4"><?php echo countBans(); ?></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="no-padding-bottom">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-6">
                <div class="checklist-block block">
                  <div class="title"><strong>Lunarbans v2</strong></div>
                  <div class="checklist">
                    <div class="item d-flex align-items-center">
                      <input type="checkbox" checked id="input-1" name="input-1" class="checkbox-template">
                      <label for="input-1">Kasutaja haldus</label>
                    </div>
                    <div class="item d-flex align-items-center">
                        <input type="checkbox" id="input-1" name="input-1" class="checkbox-template">
                        <label for="input-1">Logimis süsteem</label>
                    </div>
                    <div class="item d-flex align-items-center">
                        <input type="checkbox" checked id="input-1" name="input-1" class="checkbox-template">
                        <label for="input-1">AmxBans</label>
                    </div>
                    <div class="item d-flex align-items-center">
                      <input type="checkbox" id="input-2" name="input-2" class="checkbox-template">
                      <label for="input-2">Adminide haldus</label>
                    </div>
                    <div class="item d-flex align-items-center">
                      <input type="checkbox" id="input-3" name="input-3" class="checkbox-template">
                      <label for="input-3">Bännide haldus</label>
                    </div>
                    <div class="item d-flex align-items-center">
                      <input type="checkbox" checked id="input-4" name="input-4" class="checkbox-template">
                      <label for="input-4">SMS teenused</label>
                    </div>
                    <div class="item d-flex align-items-center">
                      <input type="checkbox" id="input-5" name="input-5" class="checkbox-template">
                      <label for="input-5">Multi color</label>
                    </div>
                    <div class="item d-flex align-items-center">
                      <input type="checkbox" id="input-6" name="input-6" class="checkbox-template">
                      <label for="input-6">Valikuline multi-lang</label>
                    </div>
                  </div>
                </div>
              </div>
        </section>
