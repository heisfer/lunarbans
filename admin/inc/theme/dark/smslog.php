<div class="page-content">
  <div class="page-header">
    <div class="container-fluid">
      <h2 class="h5 no-margin-bottom">SMS log</h2>
    </div>
  </div>
  <section class="no-padding-top no-padding-bottom">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="fa fa-mobile"></i></div><strong>Tellimusi kokku</strong>
                </div>
                <div class="number dashtext-1"><?php echo totalOrders(); ?></div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="fas fa-coins"></i></div><strong>Teenimisi</strong>
                </div>
                <div class="number dashtext-2"><?php echo countRevenue(); ?> €</div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="fas fa-sms"></i></div><strong>SMS teenuseid</strong>
                </div>
                <div class="number dashtext-3"><?php echo countSMSServices(); ?></div>
              </div>
            </div>
          </div>
  </section>

  <section class="no-padding-top no-padding-bottom">
    <div class="container-fluid">
        <div class="block margin-bottom-sm">
            <div class="title"><strong>LOG</strong></div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                             <th>Kasutaja nimi</th>
                             <th>Teenuse nimi</th>
                             <th>Vastus</th>
                             <th>Summa</th>
                             <th>Kuupäev</th>
                         </tr>
                     </thead>
                     <?php
                     $smslog = smslog();
                     foreach ($smslog as $key => $val):
                      ?>
                      <tr>
                          <td><?php echo $key+1; ?></td>
                          <td><?php echo $val['username']; ?></td>
                          <td><?php echo $val['serviceName']; ?></td>
                          <td><?php echo $val['response']; ?></td>
                          <td><?php echo $val['price']; ?> €</td>
                          <td><?php echo $val['date']; ?></td>
                      </tr>
                  <?php endforeach; ?>
                     <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
  </section>
