<div class="page-content">
  <div class="page-header">
    <div class="container-fluid">
      <h2 class="h5 no-margin-bottom">bänni nimekiri</h2>
    </div>
  </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    $(document).ready(function(){
      $('form').each(function() {
        this.reset()
      });
    });
    </script>
    <script>
    	function toggleMenu(id) {
    		var e = document.getElementById(id);
    		$(e).fadeToggle("fast");
    	}
    </script>
    <div class="clickable">
        <table class="table ">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kuupäev</th>
                    <th>Mängija</th>
                    <th>Admin</th>
                    <th>Server</th>
                    <th>Põhjus</th>
                    <th>Ketiv kuni</th>
                    <th>Kehtiv</th>
                </tr>
            </thead>
            <tbody>
                <?= result_to_html($db); ?>
            </tbody>
        </table>
        <div class="clickable" style="display: none; max-width: 400px; margin: 0 auto;">
            <table  class="table table-striped table-bordered table-hover">
                <tr class='greyrow'>
                    <th colspan="2"  >Andmed</th>
                </tr>
                <tr>
                    <td width="10%">Ban ID: </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Mängija</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">IP:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Admin:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Kehtiv alates:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Kehtiv kuni:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Staatus:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Server:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Kaart:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="10%">Bannitud varasemalt:</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
