<div class="page-content">
    <div class="page-header no-margin-bottom">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Kasutajad</h2>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index">Avaleht</a></li>
            <li class="breadcrumb-item active">Kasutajad</li>
        </ul>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="row">
            <div class="col-lg-6">
                <div class="block">
                    <div class="title"><strong class="d-block">Loo kasutaja</strong></div>
                    <div class="block-body">
                        <form action="process.php" method="post">
                            <div class="form-group">
                                <label class="form-control-label">Kasutaja nimi</label>
                                <input type="text" name="username" placeholder="Kasutaja nimi" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Parool</label>
                                <input type="password" name="passwd" placeholder="Parool" class="form-control">
                            </div>
                            <div class="form-group preva">
                                <label class="form-control-label">õigus tüüp</label>
                                <select class="form-control" name="priv">
                                    <?php foreach (privileges() as $key => $val): ?>
                                        <option value="<?php echo $val["id"]; ?>"><?php echo $val["name"]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="addAccount" value="Loo konto" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="block margin-bottom-sm">
                    <div class="title"><strong>Kasutajad</strong></div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kasutaja</th>
                                        <th>Privileegi tüüp</th>
                                        <th>Tegevus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (accounts()):
                                     foreach (accounts() as $key => $val) {
                                        ?> <tr>
                                            <td class="id"><?php echo $val["id"] ?></td>
                                            <td class="name"><?php echo $val["username"]; ?></td>
                                            <td class="privilege"><?php echo privilageToName($val["privilege_id"]) ?></td>
                                            <td class="text-center"><i class="fas fa-edit edit"  style="color: green; cursor: pointer;"></i>    <i class="fas fa-trash-alt delete"  style="color: #96281b; cursor: pointer;"></i></td>
                                        </tr>
                                    <?php }
                                else: ?>
                                <tr>
                                    <td colspan="4" style="text-align: center">Loo kasutaja</td>
                                </tr>
                            <?php endif; ?>
                                </tbody>
                            </table>
                            <div class="modal fade" id="deleteAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h5 class="modal-title" id="exampleModalLabel" style="color:red; text-align: center;"> OLED KINDEL?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p id="delete-info"></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">Ei</button>
                                            <form class="" action="process.php" method="post">
                                                <input type="hidden" name="delID" class="delID" value="">
                                                <input type="submit"  class="btn btn-danger" name="delAccount" value="Jah">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="editAccount" tabindex="-1" role="dialog" aria-labelledby="accountEdit" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h5 class="modal-title" id="accountEdit" style="text-align: center;"> Konto muutmine</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="edit-info" action="process.php" method="post">

                                            </form>
                                        </div>
                                        <div class="modal-footer">

                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<script>
$(".delete").click(function() {
    var id = $(this).closest("tr").find(".id").text();
    var name = $(this).closest("tr").find(".name").text();
    var privilege = $(this).closest("tr").find(".privilege").text();
    $('#deleteAccount').modal('show');
    $(".delID").val(id);
    var text = "Nimi: " + name + "</br>";
    text += "Privileeg: " + privilege;
    document.getElementById("delete-info").innerHTML = text;
});
$(".edit").click(function() {
    var id = $(this).closest("tr").find(".id").text();
    var name = $(this).closest("tr").find(".name").text();
    var privilege = $(this).closest("tr").find(".privilege").text();
    $(".editID").val(id);
    $('#editAccount').modal('show');
    var text = "<div class=\"form-group\">";
    text += "<label class=\"form-control-label\">Kasutaja nimi</label>";
    text += "<input type=\"text\" name=\"username\" value=" + name + " placeholder=\"Kasutaja nimi\" class=\"form-control\">";
    text += "</div>";
    text += "<div class=\"form-group\">";
    text += "<label class=\"form-control-label\">Vana Parool</label>";
    text += "<input type=\"password\" name=\"passwd\" placeholder=\"Vana parool\" class=\"form-control\">";
    text += "</div>";
    text += "<div class=\"form-group\">";
    text += "<label class=\"form-control-label\">Uus Parool</label>";
    text += "<input type=\"password\" name=\"passwd\" placeholder=\"Uus parool\" class=\"form-control\">";
    text += "</div>";
    text += "<div class=\"form-group\">";
    text += "<label class=\"form-control-label\">Kinnitus Parool</label>";
    text += "<input type=\"password\" name=\"passwd\" placeholder=\"Kinnitus parool\" class=\"form-control\">";
    text += "</div>";
    text += "<div class=\"select-info\">";
    text += "</div>";
    text += "<input type=\"hidden\" name=\"editID\" class=\"editID\">";
    text += "<input type=\"submit\"  class=\"btn btn-primary\" name=\"editAccount\" value=\"Muuda\">";
    text += "<button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\">Sulge</button>";
    document.getElementById("edit-info").innerHTML = text;





    var mylayer=$('.preva').clone();
    $( mylayer ).appendTo( ".select-info" );

});
</script>
