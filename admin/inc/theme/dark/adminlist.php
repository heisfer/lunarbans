<div class="page-content">
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Admini nimekiri</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="table-responsive table-hover block">
                <table id="deepshit" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Kasutajanimi</th>
                            <th>Õigused</th>
                            <th>Kehtiv alates</th>
                            <th>Kehtiv Kuni</th>
                            <th>Kehtiv</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
$list = adminList();
$privs = listOfGroups();
$access;
$i = 1;
foreach ($list as $keys => $value) {
    foreach ($value as $key => $val) {
        foreach ($privs as $kpriv => $priv) {
            if ($val['access'] != $priv['permissions']) {
                $access = "Muu õigus";
            } else {
                $access = $priv['rank'];
            }
        }
        if ($val['expired'] == 0) {
            $val['expired'] = "Igavesti";
            $val['status'] = '<i style="color: #badc58;" class="fas fa-check"></i>';
            $val['created'] = date('m/d/Y', $val['created']);
        } else {
            $val['expired'] = date('m/d/Y', $val['expired']);
            $val['created'] = date('m/d/Y', $val['created']);
            if ($val['expired'] == date('m/d/Y')) {
                $val['status'] = '<i style="color: #eb4d4b;" class="fas fa-times"></i>';
            } else {
                $val['status'] = '<i style="color: #badc58;" class="fas fa-check"></i>';
            }

        } ?><tr onclick="toggleMenu(<?php echo "10".$i; ?>); ">
    <td><?php echo $val['username']; ?></td>
    <td><?php echo $access; ?></td>
    <td><?php echo $val['created']; ?></td>
    <td><?php echo $val['expired']; ?></td>
    <td><?php echo $val['status']; ?></td>
    </tr>

            <?php
    }
}
 ?>

                 </tbody>
               </table>
             </div>
         </div>

  </section>
  <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="valik" aria-labelledby="valik" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="valik">Valik</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <button type="button" data-toggle="modal"  class="btn btn-primary" id="btnEdit" data-target="edit"  data-dismiss="modal" name="button">Muuda</button>
            <button type="button" class="btn btn-primary float-right" id="btnDelete" name="button">Kustuta</button>
        </div>
    </div>
  </div>
</div>
<div id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
  <div role="document" class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"><strong id="exampleModalLabel" class="modal-title">Signin Modal</strong>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet consectetur.</p>
        <form>
          <div class="form-group">
            <label>Email</label>
            <input type="email" placeholder="Email Address" class="form-control">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="Password" class="form-control">
          </div>
          <div class="form-group">
            <input type="submit" value="Signin" class="btn btn-primary">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
  $('tr').dblclick(function(){
      console.log(":D");
      $('#valik').modal('toggle');
  });
  /*$("#btnEdit").click(function(event) {
      $('#valik').modal('toggle');
      $('#edit').modal('toggle');
  });
  var timeoutId;
  /*$('tr').on('mousedown', function() {
      timeoutId = setTimeout(myFunction, 1000);
  }).on('mouseup mouseleave', function() {
      console.log(":P");
      clearTimeout(timeoutId);
  });*/
  </script>
