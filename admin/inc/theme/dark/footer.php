<footer class="footer">
  <div class="footer__block block no-margin-bottom">
    <div class="container-fluid text-center">
      <!-- Please do not remove the backlink to us unless you support us at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      <p class="no-margin-bottom">2019 &copy; LunarTech. </p>
    </div>
  </div>
</footer>

</div>
</div>
<!-- JavaScript files-->
<script src="inc/theme/dark/vendor/popper.js/umd/popper.min.js"> </script>
<script src="inc/theme/dark/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="inc/theme/dark/vendor/jquery.cookie/jquery.cookie.js"> </script>
<!--<script src="inc/theme/dark/vendor/chart.js/Chart.min.js"></script>-->
<script src="inc/theme/dark/vendor/jquery-validation/jquery.validate.min.js"></script>
<!--<script src="inc/theme/dark/js/charts-home.js"></script>-->
<script src="inc/theme/dark/js/front.js"></script>
<script>
$(document).ready(function () {
  var url = window.location;
  $('ul.list-unstyled a[href="'+ url +'"]').parent().addClass('active');
  $('ul.list-unstyled a').filter(function() {
    return this.href == url;
  }).parent().addClass('active');
});

$(document).ready(function(){
    if ($(".toastr")[0]) {
        toastr.options.progressBar = true;
        toastr.options.preventDuplicates = false;
        toastr.options.closeButton = true;

        if ($(".toastr-warning")[0]) {
            var str = $( ".toastr-warning" ).text();
            toastr.warning(str);
        } else if ($(".toastr-success")[0]) {
            var str = $( ".toastr-success" ).text();
            toastr.success(str);
        } else if ($(".toastr-info")[0]) {
            var str = $( ".toastr-info" ).text();
            toastr.info(str);
        } else if ($(".toastr-error")[0]) {
            var str = $( ".toastr-error" ).text();
            toastr.error(str);
        }
    }
});

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
function toggleMenu(id) {
    var e = document.getElementById(id);
    $(e).fadeToggle("fast");
}
</script>
</body>
</html>
