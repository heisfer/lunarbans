<?php

//_admins_servers
$sql = "CREATE TABLE amx_admins_servers (
  admin_id int(11) NULL,
  server_id int(11) NULL,
  custom_flags varchar(32) NOT NULL,
  use_static_bantime enum('yes', 'no') NOT NULL DEFAULT 'yes'
);";

//_amxadmins
$sql .= "CREATE TABLE amx_amxadmins (
  id int(12) NOT NULL auto_increment,
  username varchar(32) NULL,
  password varchar(32) NULL,
  access varchar(32) NULL,
  flags varchar(32) NULL,
  steamid varchar(32) NULL,
  nickname varchar(32) NULL,
  ashow int(11) NULL,
  created int(11) NULL,
  expired int(11) NULL,
  days int(11) NULL,
  PRIMARY KEY (id),
  KEY steamid (steamid)
);";
//_bans
$sql .= "CREATE TABLE amx_bans (
  bid int(11) NOT NULL auto_increment,
  player_ip varchar(32) NULL,
  player_id varchar(35) NULL,
  player_nick varchar(100) NULL DEFAULT 'Unknown',
  admin_ip varchar(32) NULL,
  admin_id varchar(35) NULL,
  admin_nick varchar(100) NULL DEFAULT 'Unknown',
  ban_type varchar(10) NULL DEFAULT 'S',
  ban_reason varchar(100) NULL,
  ban_created int(11) NULL,
  ban_length int(11) NULL,
  server_ip varchar(32) NULL,
  server_name varchar(100) NULL DEFAULT 'Unknown',
  ban_kicks int(11) NOT NULL DEFAULT '0',
  expired int(1) NOT NULL DEFAULT '0',
  imported int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (bid),
  KEY player_id (player_id)
);";

//_bans_edits
$sql .= "CREATE TABLE amx_bans_edits (
  `id` int(11) NOT NULL auto_increment,
  `bid` int(11) NOT NULL,
  `edit_time` int(11) NOT NULL,
  `admin_nick` varchar(32) NOT NULL DEFAULT 'unknown',
  `edit_reason` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);";

//_reasons
$sql .= "CREATE TABLE amx_reasons (
  id int(11) NOT NULL auto_increment,
  reason varchar(100) NULL,
  static_bantime int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
);";

//_reasons_set
$sql .= "CREATE TABLE amx_reasons_set (
  id int(11) NOT NULL auto_increment,
  setname varchar(32) NULL,
  PRIMARY KEY (id)
);";

//_reasons_to_set
$sql .= "CREATE TABLE amx_reasons_to_set (
  id int(11) NOT NULL auto_increment,
  setid int(11) NOT NULL,
  reasonid int(11) NOT NULL,
  PRIMARY KEY (id)
);";


//_serverinfo
$sql .= "CREATE TABLE amx_serverinfo (
  id int(11) NOT NULL auto_increment,
  timestamp int(11) NULL,
  hostname varchar(100) NULL DEFAULT 'Unknown',
  address varchar(100) NULL,
  gametype varchar(32) NULL,
  rcon varchar(32) NULL,
  amxban_version varchar(32) NULL,
  amxban_motd varchar(250) NULL,
  motd_delay int(10) NULL DEFAULT '10',
  amxban_menu int(10) NOT NULL DEFAULT '1',
  reasons int(10) NULL,
  timezone_fixx int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
);";

//_flagged
$sql .= "CREATE TABLE amx_flagged (
  `fid` int(11) NOT NULL auto_increment,
  `player_ip` varchar(32) default NULL,
  `player_id` varchar(35) default NULL,
  `player_nick` varchar(100) default 'Unknown',
  `admin_ip` varchar(32) default NULL,
  `admin_id` varchar(35) default NULL,
  `admin_nick` varchar(100) default 'Unknown',
  `reason` varchar(100) default NULL,
  `created` int(11) default NULL,
  `length` int(11) default NULL,
  `server_ip` varchar(100) default NULL,
  PRIMARY KEY (`fid`),
  KEY `player_id` (`player_id`)
);";

 ?>
