<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
function reloadadmins($ip,$rcon) {

}
function login($username, $password) {
    $db = new Database();
    $username = addslashes($username);
    $cpadmin = 0;
    $userinfo;
    $result = $db->query("SELECT * FROM lb_users WHERE username = '$username'", false);
    if($result && password_verify($password, $result['0']['password'])) {
        $id = $result[0]['privilege_id'];
        $priv = $db->query("SELECT * FROM lb_privileges WHERE id ='$id'");
        $userinfo['acp']['priv'] = $priv[0];
        unset($userinfo['acp']['priv']['name']);
        unset($userinfo['acp']['priv']['id']);
        $userinfo['acp']['username'] = $username;
        $userinfo['lang'] = $result[0]['lang'];
        $userinfo['acp']['title'] = $priv[0]['name'];
    }

    $amxbans = listOfAmxServers();
    foreach ($amxbans as $key => $value) {
        $dbnames = $value['amxbans'];
        $sql = "SELECT * FROM $dbnames.amx_amxadmins WHERE username = '$username' AND password = '$password'";
        $admin = $db->query($sql);
        foreach ($admin as $val) {
            $mod = explode("_", $dbnames);
            $mod = $mod[1];
            $userinfo['ucp'][$mod]['username'] = $val['username'];
            $userinfo['ucp'][$mod]['flags'] = $val['flags'];
            $userinfo['ucp'][$mod]['access'] = accessToName($val['access']);
            $userinfo['ucp'][$mod]['steamid'] = $val['steamid'];
            $userinfo['ucp'][$mod]['nickname'] = $val['nickname'];
            $userinfo['ucp'][$mod]['created'] = $val['created'];
            $userinfo['ucp'][$mod]['days'] = $val['days'];

        }

    }
    return $userinfo;


}
function url()
{
    if ($_SERVER['REQUEST_URI'] != "/") {
        $result = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['REQUEST_URI']
        );
    } else {
        $result = sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME']
        );
    }
    return $result;
}

// Count
function totalOrders()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_smslog";
    $result = $db->queryCount($sql);
    return $result;
}

function countRevenue()
{
    $db = new Database();
    $sql = "SELECT SUM(price) AS total FROM lb_smslog";
    $count = $db->query($sql);
    foreach ($count as $key => $value) {
        $result = $value['total'];
    }
    return $result;
}
function countSMSServices()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_sms";
    $result = $db->queryCount($sql);
    return $result;
}
function smslog()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_smslog";
    $result = $db->query($sql);
    return $result;
}
function countServers()
{
    $db = new Database();
    $res = array();
    $sql = "SELECT amxbans FROM lb_databases";
    if ($dbs = $db->query($sql)) {
        foreach ($dbs as $key => $value) {
            $dbname = $value['amxbans'];
            $list = "SELECT * FROM $dbname.amx_serverinfo";
            $result = $db->query($list);
            if ($result) {
                $res[] = $result;
            }
        }
        $size = sizeof($res);
        return $size;
    }
    if (sizeof($res) == 0 ) {
        $res = 0;
    }
    return $res;
}
function countAdmins()
{
    $db = new Database();
    $res = array();
    $sql = "SELECT amxbans FROM lb_databases";
    if ($dbs = $db->query($sql)) {
        foreach ($dbs as $key => $value) {
            $dbname = $value['amxbans'];
            $list = "SELECT * FROM $dbname.amx_amxadmins";
            $result = $db->query($list);
            if ($result) {
                $res = $result;
            }
        }
        $size = sizeof($res);
        return $size;
    }
    if (sizeof($res) == 0 ) {
        $res = 0;
    }
    return $res;
}
function countBans()
{
    $db = new Database();
    $res = array();
    $sql = "SELECT amxbans FROM lb_databases";
    if ($dbs = $db->query($sql)) {
        foreach ($dbs as $key => $value) {
            $dbname = $value['amxbans'];
            $list = "SELECT * FROM $dbname.amx_bans";
            $result = $db->query($list);
            if ($result) {
                $res = $result;
            }
        }
        $size = sizeof($res);
        return $size;
    }
    if (sizeof($res) == 0 ) {
        $res = 0;
    }
    return $res;
}

function countUsers()
{
    $db = new Database();
    $res = array();
    $sql = "SELECT username FROM lb_users";
    $users = $db->query($sql);
    $size = sizeof($users);
    return $size;
}

// Privileges section
function privileges()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_privileges";
    $result = $db->query($sql);
    return $result;
}
function createPrivilege($name, $privileges)
{
    $db = new Database();
    $name = addslashes($name);
    $tables = "";
    $values = "";
    foreach ($privileges as $key => $value) {
        $tables .= "$key, ";
        $values .= "'$value', ";
    }
    $tables = rtrim($tables,", ");
    $values = rtrim($values,", ");
    $sqlcheck = "SELECT name FROM lb_privileges WHERE name = '$name'";
    if (!$db->query($sqlcheck)) {
        $sql = "INSERT INTO lb_privileges (name, $tables) VALUES ('$name', $values)";
        if ($db->query($sql)) {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

function translatePrivilegs($id)
{
    $db = new Database();
    $sql = "SELECT privileges FROM lb_privileges WHERE id = '$id'";
    $privilege = $db->query($sql);
    $strPrivilege = implode("", $privilege["0"]);
    $privileges = preg_split('//', $strPrivilege, -1, PREG_SPLIT_NO_EMPTY);
    return $privileges;
}
function privilageToName($id)
{
    $db = new Database();
    $sql = "SELECT name FROM lb_privileges WHERE id = '$id'";
    if ($name = $db->query($sql)) {
        $result = implode("", $name["0"]);
    } else {
        $result = "unknow";
    }
    return $result;
}
function deletePrivilege($id)
{
    $db = new Database();
    $id = addslashes($id);
    if (!$db->delete("lb_privileges", "id = $id")) {
        return false;
    }
    return true;
}

function updatePrivilege($id, $name, $privileges)
{
    $db = new Database();
    $name = addslashes($name);
    $privilege = addslashes($privileges);
    $namecheck = "SELECT name FROM lb_privileges WHERE name = '$name'";
    $privcheck = "SELECT name FROM lb_privileges WHERE privileges = '$privilege'";
    if (!$db->update("lb_privileges", array("name" => $name, "privileges" => $privilege), "id = '$id'")) {
        return false;
    }
    return true;
}

//users section
function createAccount($name, $password, $privilege_id)
{
    $db = new Database();
    $name = addslashes($name);
    $password = addslashes(password_hash($password, PASSWORD_DEFAULT));

    $privilege = implode("", $privilege['0']);
    $sqlcheck = "SELECT username FROM lb_users WHERE username = '$name'";
    if (!$db->query($sqlcheck)) {
        if (!$db->insert("lb_users", array('username' => $name, 'password' => $password, 'privilege_id' => $privilege_id))) {
            return false;
        }
    } else {
        return false;
    }
    return true;
}
function deleteAccount($id)
{
    $db = new Database();
    $id = addslashes($id);
    if (!$db->delete("lb_users", "id = $id")) {
        return false;
    }
    return true;
}
function accounts()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_users";
    $result = $db->query($sql);
    return $result;
}

//AMXBANS
function addDatabase($name, $database, $version)
{
    $db = new Database();
    $name = addslashes($name);
    $database = addslashes($database);
    if ($db->createDatabase($database, $version)) {
        $result = $db->insert("lb_databases", array('servername' => $name, 'amxbans' => $database, 'version' => $version));
        if ($result) {
            return true;
        }
        return false;
    }
    return false;
}

function listOfAmxVersions()
{
    global $root;
    if ($handle = opendir("{$root}admin/inc/tabels")) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $newFileName = substr($entry, 0, (strrpos($entry, ".")));
                $fixed = substr($newFileName, 1);
                $array[] = $fixed;
            }
        }

        closedir($handle);
        return $array;
    }
    return null;
}

function listOfAmxServers()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_databases";
    $result = $db->query($sql);
    return $result;
}

function deleteAmxDatabase($id)
{
    $db = new Database();
    $sql = "SELECT * FROM lb_databases WHERE id=$id";
    $result = $db->query($sql);
    if ($result != null) {
        if ($db->dropDatabase($result[0]['amxbans'])) {
            if ($db->delete("lb_databases", "id=$id")) {
                return true;
            }
            return false;
        }
        return false;
    }
    return false;
}

function adminList()
{
    $db = new Database();
    $res = array();
    $sql = "SELECT amxbans FROM lb_databases";
    if ($dbs = $db->query($sql)) {
        foreach ($dbs as $key => $value) {
            $dbname = $value['amxbans'];
            $list = "SELECT * FROM $dbname.amx_amxadmins";
            $result = $db->query($list);
            if ($result) {
                $res[$dbname] = $result;
            }
        }
        return $res;
    }
    return $res;
}

function listOfGroups()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_adminPrivileges";
    $result = $db->query($sql);
    return $result;
}
function accessToName($priv) {
    $db = new Database();
    $sql = "SELECT * FROM lb_adminPrivileges WHERE permissions = '$priv'";
    $result = $db->query($sql);
    return $result[0]['rank'];
}
function createGroup($rank, $permission)
{
    $db = new Database();
    $sql = "SELECT * FROM lb_adminPrivileges";
    if ($db->insert("lb_adminPrivileges", array('rank' => $rank, 'permissions' => $permission))) {
        return true;
    }
    return false;
}

function serverList($dbname = null, $serverid = null)
{
    $db = new Database();
    $res = array();
    if ($dbname == null) {
        $sql = "SELECT amxbans FROM lb_databases";
        if ($dbs = $db->query($sql)) {
            foreach ($dbs as $key => $value) {
                $dbname = $value['amxbans'];
                $list = "SELECT * FROM $dbname.amx_serverinfo";
                $result = $db->query($list);
                if ($result) {
                    $res[$dbname] = $result;
                }
            }
            return $res;
        }
    } else {
        $sql = "SELECT hostname FROM $dbname.amx_serverinfo";
        if ($res = $db->query($sql)) {
            return $res;
        }
        return $res;
    }
    return $res;
}

function createAdmin($username, $password, $usertype, $access, $flags, $days, $server, $staticBan)
{
    global $csRcon;
    $db = new Database();
//    echo $username." ".$password." ".$usertype." ".$access." ".$flags." ".$days." ".$server." ".$staticBan;
    if (is_array($server)) {
        foreach ($server as $keys => $value) {
            $servers[] = json_decode($value, true);
        }
    } else {
        $servers = json_decode($server, true);
    }
    //print_r($servers);
    foreach ($servers as $val) {
        $dbname = $val['db'];
        $serverid = $val['id'];
        $sql = "SELECT *  FROM $dbname.amx_amxadmins WHERE steamid='$username'";
        $sql2 = "SELECT * FROM $dbname.amx_amxadmins WHERE steamid='$username' AND access='$access'";
        if ($result = $db->query($sql)) {
            $result2 = $db->query($sql2);
            if ($result2) {
                foreach ($result2 as $key => $value) {
                    $g_admin_expire = time()  + (int) $days*86400;
                    $old_days = $value['days'];
                    $old_exp = $value['expired'];
                    $old_created = $value['created'];
                    $total = $old_exp - $old_created;
                    $total2 = $g_admin_expire + $total;
                    $newDays = $days + $old_days;
                    $sql = "UPDATE $dbname.amx_amxadmins SET access='$access', expired='$total2', days='$newDays' WHERE steamid='$usertype'";

                    $db->_dbSql($sql);
                }
                //$csRcon->RconCommand("amx_reloadadmins");
                $update = true;
            }
            //$csRcon->RconCommand("amx_reloadadmins");

            //return 302;
        }
        if ($days != 0) {
            $exp="(UNIX_TIMESTAMP()+(".($days * 86400)."))";
        } else {
            $exp ="0";
        }
        $sql = "INSERT INTO $dbname.amx_amxadmins(`username`,`password`,`access`,`flags`,`steamid`,`nickname`,`created`,`expired`,`days`)
        VALUES (
        '".$username."',
        '".$password."',
        '".$access."',
        '".$flags."',
        '".$usertype."',
        '".$username."',
        UNIX_TIMESTAMP(),
        ".$exp.",
        ".$days."
        )";
        $adminid = $db->_dbSql($sql);
        if ($adminid) {
            $sql = "INSERT INTO $dbname.amx_admins_servers
                    (`admin_id`,`server_id`,`custom_flags`,`use_static_bantime`)
                    VALUES
                    ('".$adminid."','".$serverid."','','".$staticBan."')
                    ";
            $result = $db->_dbSql($sql);
            //echo $result;
            if ($result == "0") {
                //$csRcon->RconCommand("amx_reloadadmins");
                $done = true;
            }
            //return 304;
        }
        //return 305;
    }
    if ($done) {
        return 303;
    } elseif ($update) {
        return 301;
    }
}

//SMS
function createSMSService($serviceName, $secretKey, $number, $message, $price, $permission, $json)
{
    $db = new Database();
    if ($db->insert("lb_sms", array('serviceName' => $serviceName, 'sercretKey' => $secretKey, 'number' => $number, 'message' => $message, 'price' => $price, 'permission' => $permission, 'server' =>  "$json"))) {
        return true;
    }
    return false;
}
function deleteSMSService($id)
{
    $db = new Database();
    if ($db->delete("lb_sms", "id = $id")) {
        return true;
    }
    return false;
}
function listOfSMSServices()
{
    $db = new Database();
    $sql = "SELECT * FROM lb_sms";
    $result = $db->query($sql);
    return $result;
}
function allStatus() {
    $db = new Database();

    $banned = 0;
    $sql = "SELECT * FROM lb_databases";
    $gotBanned = 0;
    $dbnames = $db->query($sql);
    $info = array();
    $steamid_chars = '/^STEAM_/';
    foreach ($dbnames as $key => $value) {
        $dbname = $value['amxbans'];
        $sql = ("SELECT * FROM $dbname.amx_serverinfo");
        $serverinfo = $db->query($sql);
        foreach ($serverinfo as $val) {
            $hostaddr = explode(":", $val['address']);
            $ip = $hostaddr[0];
            $port = $hostaddr[1];
            $rcon = $val['rcon'];
            $info[$dbname]['info']['host'] = $val['hostname'];
            $info[$dbname]['info']['address'] = $val['address'];
            $info[$dbname]['info']['ip'] = $ip;
            $info[$dbname]['info']['port'] = $port;
            if($ip && $port) {
                $cs = new Rcon();
                    $cs->Connect($ip, $port, $rcon);
                    $csinfo = $cs->ServerInfo();
                    if ($csinfo) {
                        $info[$dbname]['info']['map'] = $csinfo['map'];
                        $info[$dbname]['info']['maxplayers'] =  $csinfo['maxplayers'];
                        $info[$dbname]['info']['activeplayers'] = $csinfo['activeplayers'];
                        if ($csinfo['activeplayers'] != "0") {
                            for ($i=1; $i <= $csinfo['activeplayers']; $i++) {
                                $info[$dbname]['info']['players'][$i] = $csinfo[$i];
                            }

                        }
                    }
                    $cs->Disconnect();
            }



        }

        $sql = ("SELECT * FROM $dbname.amx_amxadmins");
        $admins = $db->query($sql);
            $info[$dbname]['info']['totaladmins'] = @sizeof($admins);
        if (is_array($admins)) {
            foreach ($admins as $val) {
                $username = $val['username'];
                if(preg_match($steamid_chars, $val['steamid'])) {
                    $steamid = $val['steamid'];
                    $sql = "SELECT * FROM $dbname.amx_bans WHERE admin_id = '$steamid'";
                    $banned = $db->queryCount($sql);
                    $sql = "SELECT * FROM $dbname.amx_bans WHERE player_id = '$steamid'";
                    $gotBanned += $db->queryCount($sql);
                }
                else {
                    $sql = "SELECT * FROM $dbname.amx_bans WHERE admin_nick = '$username'";
                    $banned = $db->queryCount($sql);
                    $sql = "SELECT * FROM $dbname.amx_bans WHERE player_nick = '$steamid'";
                    $gotBanned = $db->queryCount($sql);
                }

                $info[$dbname]['adminlist'][$val['id']]['username'] = $val['username'];
                $info[$dbname]['adminlist'][$val['id']]['steamid'] = $val['steamid'];
                $info[$dbname]['adminlist'][$val['id']]['created'] = $val['created'];
                $info[$dbname]['adminlist'][$val['id']]['expired'] = $val['expired'];
                $info[$dbname]['adminlist'][$val['id']]['days'] = $val['days'];
                $info[$dbname]['adminlist'][$val['id']]['flags'] = $val['flags'];
                $info[$dbname]['adminlist'][$val['id']]['access'] = $val['access'];
                $info[$dbname]['adminlist'][$val['id']]['banned'] = $banned;
                $info[$dbname]['adminlist'][$val['id']]['gotBanned'] = $gotBanned;
            }
        }

        $sql = "SELECT * FROM $dbname.amx_bans";
        $bans = $db->query($sql);
        $info[$dbname]['info']['totalbans'] = @sizeof($bans);
        if (is_array($bans)) {
            foreach ($bans as $val) {
                $nick = $val['player_nick'];
                $sql = "SELECT * FROM $dbname.amx_bans where player_nick = '$nick'";
                $bannedbefore = $db->queryCount($sql);
                $bannedbefore = $bannedbefore -1;
                $info[$dbname]['banlist'][$val['bid']]['username'] = $val['player_nick'];
                $info[$dbname]['banlist'][$val['bid']]['player_ip'] = $val['player_ip'];
                $info[$dbname]['banlist'][$val['bid']]['player_id'] = $val['player_id'];
                $info[$dbname]['banlist'][$val['bid']]['created'] = $val['ban_created'];
                $info[$dbname]['banlist'][$val['bid']]['reason'] = $val['ban_reason'];
                $info[$dbname]['banlist'][$val['bid']]['type'] = $val['ban_type'];
                $info[$dbname]['banlist'][$val['bid']]['admin'] = $val['admin_nick'];
                $info[$dbname]['banlist'][$val['bid']]['length'] = $val['ban_length'];
                $info[$dbname]['banlist'][$val['bid']]['server_name'] = $val['server_name'];
                $info[$dbname]['banlist'][$val['bid']]['bannedbefore'] = $bannedbefore;

                $expired =  date('Y-m-d H:i', $val['ban_created']);
                $time = time();
                $date = date("Y-m-d H:i",$time);
                //echo "$date";
                $today_time = strtotime($date);
                $expire_time = strtotime($expired);
                if ($today_time > $expire_time) {
                    $info[$dbname]['banlist'][$val['bid']]['expired'] = "1";
                } elseif($val['ban_length'] == "0") {
                    $info[$dbname]['banlist'][$val['bid']]['expired'] = "0";
                }
                else {
                    $info[$dbname]['banlist'][$val['bid']]['expired'] = "0";
                }
                //echo date('Y-m-d H:i',strtotime('+'$val['ban_length']' minutes',strtotime($start)));
            }
        }
    }
    return $info;
}
function logSystem($action, $username = null)
{
    $db = new Database();
    if (!isset($_SESSION['username'])) {
        $username = "System";
    }
    $db->insert("lb_logs", array('username' => $username, 'action' => $action));
}
function preEcho($array) {
    echo '<pre>'; // Eelvormindatud tekst
    print_r($array);
    echo '</pre>';

}
