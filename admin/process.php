<?php
/**
 *
 */
session_start();
class Process
{

    function process() {
        $arrPost = $_POST;
        end($arrPost);
        $post = key($arrPost);
        if(isset($post) || $post != null) {
            $this->$post();
        } else {
            $this->logout();
        }
    }
    function logout() {
        $_SESSION = array();
        $_SESSION['error']['alert'] = "Logisid edukalt välja!";
        $_SESSION['error']['success'] = true;
        header("Location: ".$_SERVER["HTTP_REFERER"]);
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }
    function login() {
        $username = $_POST['username'];
        $password = $_POST['password'];
        print_r($_POST);
        $result = login($username, $password);
        if ($result) {
            $_SESSION = $result;
            $_SESSION['error']['alert'] = "Logisid edukalt sisse!";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Kasutaja andmed on valed!";
            $_SESSION['error']['success'] = false;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }
    function addPrivilege() {
        //$privileges = implode("", $_POST['privilege']); // teeb massiivi stringiks

        $name = $_POST['privName'];
        unset($_POST['addPrivilege']);
        unset($_POST['privName']);
        $privileges = $_POST;
        $result =  createPrivilege($name, $privileges);
        if($result) {
            $_SESSION['error']['alert'] = "Privileegi tüüp  on loodud!";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Privileegi funktsioon on vigane või sa tegid midagi valesti :(";
			$_SESSION['error']['success'] = false;
			header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }
    function addAccount() {
        $name = $_POST['username'];
        $passwd = $_POST['passwd'];
        $privilege_id = $_POST['priv'];
        $result =  createAccount($name, $passwd, $privilege_id);
        if($result) {
            $_SESSION['error']['alert'] = "Kasutaja " . $name . " on loodud!";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "kasutaja funktsioon on vigane või sa tegid midagi valesti :(";
			$_SESSION['error']['success'] = false;
			header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }
    function delPriv() {
        $id = $_POST['delID'];
        $result = deletePrivilege($id);
        if($result) {
            $_SESSION['error']['alert'] = "Privileeg on edukalt kustutatud ";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Privileegi funktsioon on vigane või sa tegid midagi valesti :(";
			$_SESSION['error']['success'] = false;
			header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }
    function getPrivileges() {
        $id = $_POST['privID'];
        $result = translatePrivilegs($id);
        echo json_encode($result);
    }
    function editPriv() {
        $privileges = implode("", $_POST['privilege']);
        $name = $_POST['privName'];
        $id = $_POST['editID'];
        $result = updatePrivilege($id, $name, $privileges);
        if($result) {
            $_SESSION['error']['alert'] = "Privileeg on edukalt uuendatud ";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Privileegi funktsioon on vigane või sa tegid midagi valesti :(";
			$_SESSION['error']['success'] = false;
			header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }
    function delAccount() {
        $id = $_POST['delID'];
        $result = deleteAccount($id);
        if($result) {
            $_SESSION['error']['alert'] = "Konto on edukalt kustutatud ";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "konto funktsioon on vigane või sa tegid midagi valesti :(";
			$_SESSION['error']['success'] = false;
			header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }

    function createAmxBans() {
        $prefix = "lb_";
        $serverName = $_POST['serverName'];
        $serverDatabase = $_POST['serverDatabase'];
        $database = $prefix.$serverDatabase;
        $version = $_POST['version'];
        $result = addDatabase($serverName, $database, $version);
        $meh = $result;
        if($result) {
            $_SESSION['error']['alert'] = "$database on loodud";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "$database tahab keppi";
			$_SESSION['error']['success'] = false;
			header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }

    function delDatabase() {
        $id = $_POST['delID'];
        $result = deleteAmxDatabase($id);
        if($result) {
            $_SESSION['error']['alert'] = "Andmebaas on läinud :(";
            $_SESSION['error']['success'] = true;
			header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Andmebaas ei taha ära minna :/";
			$_SESSION['error']['success'] = false;
			header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }

    function addGroup() {
        $rank = $_POST['rank'];
        $permission = $_POST['permission'];
        $result = createGroup($rank, $permission);
        if($result) {
            $_SESSION['error']['alert'] = "Õigused on loodud";
            $_SESSION['error']['success'] = true;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Õigused on noobidele";
            $_SESSION['error']['success'] = false;
            header("Location: ".$_SERVER["HTTP_REFERER"]); # Juhul kui on valed logimise andmed
        }
    }

    function addAdmin() {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $usertype = $_POST['usertype'];
        $access = $_POST['access'];
        $flags = $_POST['flags'];
        $days = $_POST['days'];
        $server = $_POST['server'];
        $staticBan = $_POST['staticBan'];

        $result = createAdmin($username,$password,$usertype,$access,$flags,$days,$server,$staticBan);
        //print_r($result);
        $log = "$username lisati õigus";
        logSystem($log);
        if($result) {

            $_SESSION['error']['alert'] = "Jumal on loodud";
            $_SESSION['error']['success'] = true;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Mingi hälvik oled v";
            $_SESSION['error']['success'] = false;
            echo "mfv";
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }
    function addsms() {
        echo "<pre>";
        print_r($_POST);
        $serviceName = $_POST['serviceName'];
        $secretKey = $_POST['secretKey'];
        $number = $_POST['number'];
        $message = $_POST['message'];
        $price = $_POST['price'];
        $permission = $_POST['permission'];
        $server = $_POST['server'];
        print_r(sizeof($server));
        $json = "[";
        foreach ($server as $key => $value) {
            $json .= $value.",";
        }
        $json = rtrim($json,",");
        $json .= "]";
        $result = createSMSService($serviceName, $secretKey, $number, $message, $price, $permission, $json);
        $log = "$serviceName SMS teenus on lisatud";
        logSystem($log);
        if($result) {
            $_SESSION['error']['alert'] = "SMS teenused on loodud";
            $_SESSION['error']['success'] = true;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Andmed pole õiged";
            $_SESSION['error']['success'] = false;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }
    function amxdb() {
        $db = new Database();
        $amxdb = $_POST['amxdb'];
        $result = $db->exportdb($amxdb);
        $_SESSION['error']['alert'] = $result; //'Database <b>'. $database .'</b> successfully exported to <b>'. $filename .'</b>';
        $_SESSION['error']['success'] = true;
        header("Location: ".$_SERVER["HTTP_REFERER"]);
        echo $amxdb;
        /*switch ($result) {
            case 0:
                $_SESSION['error']['alert'] = $result; //'Database <b>'. $database .'</b> successfully exported to <b>'. $filename .'</b>';
                $_SESSION['error']['success'] = true;
                header("Location: ".$_SERVER["HTTP_REFERER"]);
                break;

            case 1:
                $_SESSION['error']['alert'] = 'There was a warning during the export of <b>'. $database .'</b> to <b>'. $filename .'</b>';
                $_SESSION['error']['success'] = false;
                header("Location: ".$_SERVER["HTTP_REFERER"]);
                break;

            default:
                $_SESSION['error']['alert'] = $result;
                $_SESSION['error']['success'] = false;
                header("Location: ".$_SERVER["HTTP_REFERER"]);
                break;
            }*/
    }
}

$run = new Process();
