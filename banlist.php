<?php
$banned = false;
$ipBannedBefore = false;
$ip = $_SERVER['REMOTE_ADDR'];
$a = 0;
foreach ($banlist as $key => $value):
    foreach ($value as $keys => $val):
        if ($banned == false):
            if ($val['created'] > ($val['created'] + $val['length'] * 60)) {
                if ($_SERVER['REMOTE_ADDR'] == $val['player_ip']):
                    $banned = true;
                endif;
            }
        endif;
        if ($_SERVER['REMOTE_ADDR'] == $val['player_ip']):
            $ipBannedBefore = true;
        endif;
    endforeach;
    $a++;
endforeach;
$i = 4;
if ($banned == true):?>
<div class="card bg-danger mt-5">
<div class="card-body text-center">
IP address: <?php echo $_SERVER['REMOTE_ADDR']; ?> - Sinu IP on banned!
</div>
</div>
<?php elseif($ipBannedBefore == true): ?>
<div class="card bg-warning mt-5">
<div class="card-body text-center">
IP address: <?php echo $_SERVER['REMOTE_ADDR']; ?> -  Sinu IP ei ole banned, aga on varasemalt bannitud!
</div>
</div>
 <?php else: ?>
     <div class="card bg-success mt-5">
  <div class="card-body text-center">
    IP address: <?php echo $_SERVER['REMOTE_ADDR']; ?> - Sinu IP ei ole banned!
  </div>
</div>
<?php endif; ?>
    <div id="servers" class="text-center mt-3">
        <?php foreach ($banlist as $key => $value): ?>
            <?php if ($key == 0): ?>
                <button type="button" class="btn btn-servers btn-primary" id="<?php echo "$key"; ?>"><?php echo $key; ?></button>
            <?php else: ?>
                <button type="button" class="btn btn-servers btn-light" id="<?php echo "$key"; ?>"><?php echo $key; ?></button>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php foreach ($banlist as $keys => $value): ?>
    <table class="table table-hover">
        <thead class="black text-white">
            <tr>
                <th>Kuupäev</th>
                <th>Mängija</th>
                <th>Põhjus</th>
                <th>Kehtiv kuni</th>
                <th class="text-center">Kehtiv</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($value as $key => $val):
                $sub = substr($val['server_name'], strpos($val['server_name'], '(') + 1, strlen($val['server_name']));
                $ban_map = substr($sub, 0, strpos($sub, ')'));
                ?>
                <tr  onclick="toggleMenu('<?php echo $val['username'].$i; ?>')">
                    <td><?php echo date("d-m-Y, h:i:s",$val['created']); ?></td>
                    <td><?php echo $val['username']; ?></td>
                    <td><?php echo $val['reason']; ?></td>
                    <?php $length = strtotime('+ ' . $val['length'] . ' minutes', $val['created']); ?>
                    <?php //echo $length ?>
                    <br>
                    <?php //echo $val['created']; ?>
                    <td><?php echo  date("d-m-Y, h:i:s", $length); ?></td>
                    <td class="text-center">
                        <?php if ($val['created'] > $length): ?>
                            <i class="fas fa-mobile-alt text-success" data-toggle="tooltip" title="Ainult SMS teenusega"></i>
                        <?php else: ?>
                            <i class="fas fa-times  text-danger" data-toggle="tooltip" title="ban on läbi saanud"></i>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr id="<?php echo $val['username'].$i; ?>" style="display:none">
                    <td colspan="5" >
                        <div class="adminlist-hidden" style="max-width: 80%; margin: 0 auto; padding: 0">
                        <table style="margin:0; width:100%;">
                            <thead class="gray grey lighten-2">
                                <tr>
                                    <th colspan="2"><?php echo $val['username'];?> lisainfo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:20%">Admini nimi</td>
                                    <td style="width:80%"><?php echo $val['admin']; ?></td>
                                </tr>
                                <tr>
                                    <td style="width:20%">Kaart</td>
                                    <td style="width:80%"><?php echo $ban_map ?></td>
                                </tr>
                                <tr>
                                    <td style="width:20%">Bännitud enne</td>
                                    <td style="width:80%"><?php echo $val['bannedbefore']; ?> korda</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </td>
                </tr>

            <?php $i++; endforeach; ?>

        </tbody>
<?php endforeach; ?>

            </table>
