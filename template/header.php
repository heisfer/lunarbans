<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title><?= $title  ?></title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="admin/inc/theme/dark/vendor/toastr/css/toastr.min.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="https://lb.lunar.icu">LunarBans</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item <?= $admin ?>">
                <a class="nav-link" href="index">Admin List</a>
              </li>
              <li class="nav-item <?= $ban ?>">
                <a class="nav-link" href="banlist">Ban List</a>
              </li>
              <li class="nav-item <?= $server ?>">
                <a class="nav-link" href="server">Server List</a>
              </li>
            </ul>
            <?php if (isset($_SESSION['ucp']) && isset($_SESSION['acp'])) :?>
                <ul class="navbar-nav">
                    <li class="nav-item" style="text-align: right;">
                        <a class="nav-link" href="admin/index">Admin KP</a>
                    </li>
                    <li class="nav-item" style="text-align: right;">
                        <a class="nav-link" href="user">USER KP</a>
                    </li>
                    <li class="nav-item" style="text-align: right;">
                        <a class="nav-link" href="admin/process.php">Logout</a>
                    </li>
                </ul>
            <?php elseif (isset($_SESSION['acp'])) :?>
                    <ul class="navbar-nav">
                        <li class="nav-item" style="text-align: right;">
                            <a class="nav-link" href="admin/index">Admin KP</a>
                        </li>
                        <li class="nav-item" style="text-align: right;">
                            <a class="nav-link" href="admin/process.php">Logout</a>
                        </li>
                    </ul>
            <?php elseif (isset($_SESSION['ucp'])) :?>
                    <ul class="navbar-nav">
                        <li class="nav-item" style="text-align: right;">
                            <a class="nav-link" href="user">USER KP</a>
                        </li>
                        <li class="nav-item" style="text-align: right;">
                            <a class="nav-link" href="admin/process.php">Logout</a>
                        </li>
                    </ul>
            <?php else : ?>
                <form class="form-inline my-2 my-lg-0" action="admin/process.php" method="post">
                  <input class="form-control mr-sm-2" type="text" name = "username" placeholder="Username">
                  <input class="form-control mr-sm-2" type="password" name = "password" placeholder="Password">
                  <input type="hidden" name="login" value="1">
                  <button class="btn btn-secondary my-2 my-sm-0" type="submit">Login</button>
                </form>
            <?php endif; ?>
          </div>
        </nav>

        <div class="container">
