</div>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/js/mdb.min.js"></script>
<script src="admin/inc/theme/dark/vendor/toastr/js/toastr.min.js"></script>
<script type="text/javascript">
$("#servers .btn-servers").click(function() {
    $(".btn-servers").removeClass("btn-primary");
    $(".btn-servers").addClass("btn-light")
    $(this).addClass("btn-primary");
    $(this).removeClass("btn-light");
    $('[id*="1"]').css('display', 'none');
    $('#'+this.id+'1').css('display', 'table');
    console.log('#'+this.id+'1');

});
$("button.table-btn").on("click", function(){
    var name;

    /*$(".table-btn").removeClass("text-primary");
    $(".table-btn").addClass("text-light");
    $(this).addClass("text-primary");
    $(this).removeClass("text-light");
    console.log("test");
    var name = $(this).attr("name");
    $('[id*="TableCheck"]').css('display', 'none');
    $('#TableCheck'+name).css('display', 'table');
    $('[id*="answersChecker"]').css('display', 'none');
    $('#answersChecker'+name).css('display', 'block');
    console.log(name);*/
});
function toggleMenu(id) {
    var e = document.getElementById(id);
    $(e).fadeToggle("slow")
}
$(document).ready(function(){
    if ($(".toastr")[0]) {
        toastr.options.progressBar = true;
        toastr.options.preventDuplicates = false;
        toastr.options.closeButton = true;

        if ($(".toastr-warning")[0]) {
            var str = $( ".toastr-warning" ).text();
            toastr.warning(str);
        } else if ($(".toastr-success")[0]) {
            var str = $( ".toastr-success" ).text();
            toastr.success(str);
        } else if ($(".toastr-info")[0]) {
            var str = $( ".toastr-info" ).text();
            toastr.info(str);
        } else if ($(".toastr-error")[0]) {
            var str = $( ".toastr-error" ).text();
            toastr.error(str);
        }
    }
});
$(".ucp-menu").click(function() {
    $('div[class*="ucp"]').css('display', 'none');
    $("."+this.id).css('display', 'block');
    $(".ucp-menu").removeClass("active");
    $("#"+this.id).addClass("active");
});
</script>
</body>
</html>
