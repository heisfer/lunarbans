<?php
$ucp = $_SESSION['ucp'];
    ?>


    <h4 class="mt-5">Kasutaja KP</h4>
    <div class="card text-center ">
      <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
          <li class="nav-item">
            <a class="nav-link active ucp-menu" id="ucp-status">Info</a>
          </li>
          <li class="nav-item">
            <a class="nav-link ucp-menu" id="ucp-settings">Seaded</a>
          </li>
          <li class="nav-item">
            <a class="nav-link  ucp-menu" id="ucp-ranks"> Rankid</a>
          </li>
          <li class="nav-item">
            <a class="nav-link  ucp-menu" id="ucp-services">Teenuse ajalugu</a>
          </li>
        </ul>
      </div>
      <div class="ucp-status">
          <div class="card-deck">
          <?php $i =0; foreach ($ucp as $key => $val):
              $username = $val['username'];
              $server = $key;
              if ($val['days'] == 0) {
                  $val['days'] = "Igavesti";
                  $val['status'] = '<i style="color: #badc58;" class="fas fa-check"></i>';
                  $val['created'] = date('m-d-Y', $val['created']);
              } else {
                  $val['days'] = date('m-d-Y', $val['days']);
                  $val['created'] = date('m-d-Y', $val['created']);
                  if ($val['days'] == date('m/d/Y')) {
                      $val['status'] = '<i style="color: #eb4d4b;" class="fas fa-times"></i>';
                  } else {
                      $val['status'] = '<i style="color: #badc58;" class="fas fa-check"></i>';
                  }
              } $i++;
              ?>
              <div class="card-body">
                  <div class="card">
                    <div class="card-body">
                      <h5 class="card-title text-left"><?php echo $server; ?></h5>
                      <hr>
                      <p class="text-left">
                          <b>Rank:</b> <?php echo $val['access']; ?> <br>
                          <b>Kehtiv alates:</b> <?php echo $val['created']; ?><br>
                          <b>Kehtiv kuni:</b> <?php echo $val['days']; ?> <br>
                          <b>Staatus: </b> <?php echo $val['status']; ?>
                      </p>
                    </div>
                  </div>
              </div>

          <?php endforeach; ?>
          </div>
          </div>
          <div class="ucp-settings m-5 text-left" style="display:none">
              <label for="editPassword" class="mt-3">Parooli muutmine</label>
              <input type="text" id="editPassword" class="form-control">
              <label for="editName"  class="mt-3">Nime muutmine</label>
              <input type="text" id="editName" class="form-control">
              <label for="rank"  class="mt-3">Millele admin</label>
              <select id="rank" class="custom-select custom-select-sm">
                <option selected>Open this select menu</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
          </div>
          <div class="ucp-ranks" style="display:none">
              This is a ranks page
          </div>
          <div class="ucp-services" style="display:none">
              This is a services page
          </div>


    </div>
