<?php
/**
 *
 *
 *
 */


/**
 *
 */
class ClassName
{
    /**
     * Current version of Lunarbans
     *
     * @var string
     */
    public $version = "1.0.0";
    /**
	 * Current version of Lunarbans in integer
	 *
	 * @var integer
	 */
    public $version_code = 1000;
    /**
     * input information from mr.bannana nananananana bannana.
     *
     * @var array
     */
     public $input = array();
     /**
 	 * Cookie variables from grandmom... thanks grandmom :P
 	 *
 	 * @var array
 	 */
 	public $cookies = array();
    /**
	 * Information about the current user.
	 *
	 * @var array
	 */
	public $user = array();
    /**
	 * Information about the current usergroup.
	 *
	 * @var array
	 */
	public $usergroup = array();
    /**
	 * Lunarbans settings.
	 *
	 * @var array
	 */
	public $settings = array();
    /**
	 * Lunarbans configuration.
	 *
	 * @var array
	 */
	public $config = array();
    /**
	 * The request method that called this page.
	 *
	 * @var string
	 */
	public $request_method = "";
    /**
	 * Constructor of class.
	 */
    function __construct()
    {
        // Set up Lunarbans
        

    }
}

?>
