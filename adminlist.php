
<div class="mt-5">

</div>
<div id="servers" class="text-center">
    <?php foreach ($servers as $key => $value): ?>
        <?php if ($key == 0): ?>
            <button type="button" class="btn btn-servers btn-primary" id="<?php echo "$value"; ?>"><?php echo $value; ?></button>
        <?php else: ?>
            <button type="button" class="btn btn-servers btn-light" id="<?php echo "$value"; ?>"><?php echo $value; ?></button>
        <?php endif; ?>

    <?php endforeach; ?>


</div>
<?php $i = 1 ?>
<?php foreach ($adminlist as $keys => $value) { ?>
<?php if ($i == 1):
    ?><table id="<?php echo $keys."1" ?>" class="mt-4 table ">
    <?php else: ?>
        <table id="<?php echo $keys."1" ?>" class="mt-4 table table-hover" style="display:none">

<?php endif; ?>

    <thead class="black white-text">
        <tr>
            <th>Kasutajanimi</th>
            <th>Õigused</th>
            <th>Kehtiv alates</th>
            <th>Kehtiv kuni</th>
            <th>Kehtiv</th>
        </tr>
    </thead>
    <tbody>
        <?php
            
            foreach ($value as $key => $val) {
                $i++;
                foreach ($privs as $kpriv => $priv) {

                    if ($val['access'] != $priv['permissions']) {
                        $access = "Muu õigus";
                    } else {
                        $access = $priv['rank'];
                    }


                }
                $created = date('d.m.Y', $val['created']);
                if ( $val['expired'] == 0 ||  $val['expired'] == null) {
                    $exp = "Igavene";
                    $status = true;
                } else  {
                    $exp = date('d.m.Y', $val['expired']);
                    if ($created >= $exp) {
                        $status = false;
                    }
                }
                ?>
                <tr onclick="toggleMenu('<?php echo $val['username'].$i; ?>')">
                    <td><?php echo $val['username']; ?></td>
                    <td><?php echo $access; ?></td>
                    <td><?php echo $created; ?></td>
                    <td><?php echo $exp; ?></td>
                    <td><?php echo $status ? "<i style=\"color: green\" class=\"fa fa-check\"></i>" : "<i style\"color: red\" class=\"fas fa-times\"></i>" ?></td>
                </tr>
                <tr id="<?php echo $val['username'].$i; ?>" style="display:none">
                    <td colspan="5" >
                        <div class="adminlist-hidden" style="max-width: 80%; margin: 0 auto; padding: 0">
                        <table style="margin:0; width:100%;">
                            <thead class="gray grey lighten-2">
                                <tr>
                                    <th colspan="2"><?php echo $val['username'];?> lisainfo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:20%">Banni saanud</td>
                                    <td style="width:80%"><?php echo $val['gotBanned']; ?> korda</td>
                                </tr>
                                <tr>
                                    <td style="width:20%">Banninud</td>
                                    <td style="width:80%"><?php echo $val['banned']; ?> korda</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </td>
                </tr>

                <?php

            }
     ?>
    </tbody>
</table>
<?php $i++; ?>
<?php } ?>
<pre>

<?php// print_r($list); ?>
