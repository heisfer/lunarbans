<?php
require 'inc/config.inc.php';
require $root.'admin/inc/functions.php';
$list = listOfSMSServices();
$service_id = $_GET['service_id'];
$status = false;
foreach ($list as $key => $value) {
    if ($service_id == $value['service_id']) {
        $serviceName = $value['serviceName'];
        $json = $value['server'];
        $permission = $value['permission'];
        $flags = $value['flag'];
        $days = $value['days'];
        $status = true;
    }
}
if ($status) {
    function createRandomPassword() {
        $chars = "abcdefghijkmnopqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '' ;
        while ($i <= 6) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;

    }
    $name = $_GET['message'];

    $staticBan = "yes";
    if ($_GET['test'] != true) {
        $price = $_GET['price'];
        $earn = $price / 2;
    }

    //echo $name." ".$server." ".$permission." ".$flags;
    $password = createRandomPassword();
    $reply_noname = "LB SMS ERROR: Te ei sisestanud nime!";
    $replay_error = "Teenusel on midagi viga... pöördu administraatori juurde";
    $replay_success = "Te tellisite endale $serviceName paketti! \n Kasutaja nimi: $name \n Parool: $password";
    $replay_success_update = "$serviceName pakett on uuendatud!";
    $replay_success_query = "Pakett $serviceName \ võta omanikuga ühendust, kui soovid, et see aktiveeruks";
    if (!$name) {
        echo $reply_noname;
    }

    $result = createAdmin($name, $password, $name, $permission, $flags, $days, $json, $staticBan);
    //echo $result;
    $db = new Database();
    switch ($result) {
        case 301:
            if ($_GET['test'] != true) {
                $db->insert("lb_smslog", array('username' => $name, 'serviceName' => $serviceName, 'response' => $replay_success_update, 'price' => $earn));
            }

            echo $replay_success_update;
            break;
        case 302:
            if ($_GET['test'] != true) {
                $db->insert("lb_smslog", array('username' => $name, 'serviceName' => $serviceName, 'response' => $replay_success_query, 'price' => $earn));
            }

            echo $replay_success_query;
            break;
        case 303:
            if ($_GET['test'] != true) {
                $db->insert("lb_smslog", array('username' => $name, 'serviceName' => $serviceName, 'response' => $replay_success, 'price' => $earn));
            }
            echo $replay_success;
            break;
        default:
            $db->insert("lb_smslog", array('username' => $name, 'serviceName' => $serviceName, 'response' => $replay_error));
            echo $replay_error;
            break;
    }
}
