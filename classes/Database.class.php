<?php
class Database
{
    public $_connection;

    private $_host;

    private $_user;

    private $_pass;

    private $_dbName;

    private $_port;

    public function __construct()
    {
        global $db_host,$db_user,$db_password,$db_database;

        $this->_host = $db_host;
        $this->_user = $db_user;
        $this->_pass = $db_password;
        $this->_dbName = $db_database;
        $this->_port = 3306;

        try {
            $this->_connect();
            $this->_disconnect();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    public function _connect()
    {
        $this->_connection = mysqli_connect($this->_host, $this->_user, $this->_pass, $this->_dbName, $this->_port);
        if (! $this->_connection) {
            throw new mysqli_sql_exception();
        }

        mysqli_set_charset($this->_connection, "utf8");
    }

    public function _disconnect()
    {
        if ($this->_connection) {
            mysqli_commit($this->_connection);
            mysqli_close($this->_connection);
        }
    }

    public function _dbSql($sql)
    {

        $this->_connect();
        mysqli_query($this->_connection, $sql);
        $lastID = mysqli_insert_id($this->_connection);
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($error) {
            $meh =  "$error \n SQL : $sql";
            //throw new mysqli_sql_exception();
            throw new $meh;
        }

        return $lastID;
    }

    public function queryBool($qry, $showErrors = true) {
        $this->_connect();
        $result = mysqli_query($this->_connection, $qry);
        $this->_disconnect();
        return $result;
    }

    public function queryCount($qry, $showErrors = true)
    {
        $res = array();
        $count = 0;
        $this->_connect();
        $result = mysqli_query($this->_connection, $qry);
        if ($result) {
            $count = mysqli_num_rows($result);
            return $count;
        }
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($showErrors && $error) {
        //    throw new mysqli_sql_exception();
            throw new Exception($qry);
        }
        return $count;

    }


    public function query($qry, $showErrors = true)
    {
        $res = array();

        $this->_connect();
        $result = mysqli_query($this->_connection, $qry);
        if ($result) {
            $count = mysqli_num_rows($result);
            if ($count > 0) {
                while ($r = mysqli_fetch_assoc($result)) {
                    $res[] = $r;
                }
                $this->_disconnect();


                return $res;
            }
        }
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($showErrors && $error) {
        //    throw new mysqli_sql_exception();
            throw new Exception($qry);
        }


        return null;
    }


    public function multi_query($qry, $showErrors = true)
    {
        $this->_connect();
        $result = mysqli_multi_query($this->_connection, $qry);
        if ($result) {
            return true;
        }
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($showErrors && $error) {
            throw new Exception($qry);
        }


        return false;
    }
/*    public function multiDbSelect($table, $row, $table2)
    {
        $res = array();
        $sql = "SELECT $row FROM $table";
        $backup = $this->_dbName;
        if ($dbs = $this->query($sql)) {
            foreach ($dbs as $key => $value) {
                $db = $value[$row];
                $sql = "SELECT * FROM $db.$table2";
                $result = $this->query($sql);

                if ($result != null) {
                        $res[$db] = $result;
                }
            }
            return $res;
        }
        return $res;
    }

    public function multiDbInsert($db = null, $table, $row, $table2)
    {
        $res = array();
        $sql = "SELECT $row FROM $table";
        $backup = $this->_dbName;
        if ($dbs = $this->query($sql)) {
            foreach ($dbs as $key => $value) {
                $db = $value[$row];
                $sql = "SELECT * FROM $db.$table2";
                $result = $this->query($sql);

                if ($result != null) {
                        $res[$db] = $result;
                }
            }
            return $res;
        }
        return $res;
    }
*/
    public function createDatabase($database, $version = null) {
        if (!$this->checkIfDatabaseExist($database)) {
            $sqlDb = "CREATE DATABASE IF NOT EXISTS $database";
            if($this->queryBool($sqlDb)) {
                $bacupdb = $this->_dbName;
                $this->_dbName = $database;
                include '../admin/inc/tabels/v'.$version.'.php';
                $this->multi_query($sql);
                $this->_dbName = $bacupdb;
                return true;
            }
            return false;
        }
        return false;
    }

    public function dropTable($table) {
        $sql = "DROP table $table";
        if ($this->queryBool($sql)) {
            return true;
        }
        return false;
    }

    public function dropDatabase($database) {
        if($this->checkIfDatabaseExist($database)) {
            $sql = "DROP DATABASE $database";
            if($this->queryBool($sql)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function checkIfDatabaseExist($database)
    {


        $sql = "SHOW DATABASES LIKE '$database'";

        $result = $this->query($sql);

        if ($result) {
            return true;
        }

        return false;
    }

    public function checkIfTableExists($table)
    {
        $res = $this->query("SELECT table_name FROM information_schema.tables WHERE table_schema = '{$this->_dbName}' AND table_name = '$table';");
        if ($res) {
            return true;
        }

        return false;
    }
    public function insert($table, $data)
    {
        $sql = "INSERT INTO $table(";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ") VALUES(";
        foreach ($data as $i => $k) {
            $sql .= "'" . $k . "'" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ")";

        return $this->_dbSql($sql);
    }

    /*
     * return null
     */
    public function update($table, $data, $condition)
    {
        $sql = "UPDATE $table SET ";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`='" . $k . "', ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= " WHERE $condition";
        $this->_dbSql($sql);

        return true;
    }

    public function delete($table, $where = "")
    {
        $sql = "DELETE FROM `$table`";
        if ($where) {
            $sql .= " where $where";
        }
        $this->_dbSql($sql);

        return true;
    }

    public function exportdb($database) {
        global $db_host,$db_user,$db_password, $root, $siteurl;
        $filename = $root."sqlbackup/".$database.".sql";
        $url = "{$siteurl}/sqlbackup/{$database}.sql" ; // URL of the file you want to download
        $fn1 = basename($url); // Getting the base name of the file.
        $worder = null;
        $output = array();
        $fp = fopen( $filename, 'w+' );
        if( !$fp ) {
            echo "a";
        	echo 'Impossible to create <b>'. $filename .'</b>, please manually create one and assign it full write privileges: <b>777</b>';
        	exit;
        }
        echo "a";
        fclose( $fp );
        $command = 'mysqldump --opt -h '. $db_host .' -u '. $db_user .' -p'. $db_password .' '. $database .' > '. $filename;
        exec( $command, $output, $worked );
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($filename));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        ob_clean();
        flush();
        readfile($filename);
        switch ($worder) {
            case 0:
                return "$database edukat varustatud!";
            case 1:
            $error = 'There was an error during import.'
                . 'Please make sure the import file is saved in the same folder as this script and check your values:'
                . '<br/><br/><table>'
                . '<tr><td>MySQL Database Name:</td><td><b>'. $database .'</b></td></tr>'
                . '<tr><td>MySQL User Name:</td><td><b>'. $db_user .'</b></td></tr>'
                . '<tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr>'
                . '<tr><td>MySQL Host Name:</td><td><b>'. $db_host .'</b></td></tr>'
                . '<tr><td>MySQL Import Filename:</td><td><b>'. $filename .'</b></td>'
                . '</tr></table>';
                return $error;
                break;

            default:
                // code...
                break;
        }
        return $worked;
    }
}
