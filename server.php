            <table class="table mt-5">
                <thead class="black text-white">
                    <tr class="">
                        <th>Serveri nimi</th>
                        <th>IP</th>
                        <th>Mängijad</th>
                        <th>Kaart</th>
                    </tr>
                </thead>
                <tbody>

                    <?php //print_r($serverlist); ?>
                    <?php foreach ($serverlist as $val): ?>
                        <?php
                        $arrHost = explode("| ", $val['host']);
                        $mod = $arrHost[1];
                         ?>
                        <tr onclick="toggleMenu('<?php echo $mod; ?>')">
                            <td><?php echo $val['host']; ?></td>
                            <td><?php echo $val['address']; ?></td>
                            <td><?php echo isset($val['activeplayers']) ? $val['activeplayers'] : "null"; echo " / "; echo isset($val['maxplayers']) ? $val['maxplayers'] : "null" ?></td>
                            <td><?php echo isset($val['map']) ? $val['map'] : "null" ; ?></td>
                        </tr>
                        <tr id="<?php echo $mod; ?>" style="display:none">
                            <td colspan="5" >
                                <div class="adminlist-hidden" style="max-width: 80%; margin: 0 auto; padding: 0">
                                <table style="margin:0; width:100%;">
                                    <?php if ($val['activeplayers'] != 0): ?>
                                    <thead class="gray grey lighten-2">
                                        <tr>
                                            <th>Nimi</th>
                                            <th>Aeg olnud serveris</th>
                                            <th>Frags</th>
                                            <th>Ping</th>
                                        </tr>
                                    </thead>
                                    <?php endif; ?>

                                    <tbody>
                                        <?php if ($val['activeplayers'] != 0): ?>
                                            <?php foreach ($val['players']  as $value): ?>
                                                <tr>
                                                    <td><?php echo $value['name']; ?></td>
                                                    <td><?php echo $value['time']; ?></td>
                                                    <td><?php echo $value['frag']; ?></td>
                                                    <td><?php echo $value['ping']; ?>ms</td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <?php else: ?>
                                                <tr>
                                                    <td colspan=5 class="bg-warning text-center">Serveris pole ühtegi inimest :/</td>
                                                </tr>
                                        <?php endif; ?>
                                </table>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
<pre>

<?php
//print_r($serverlist);
 ?>
