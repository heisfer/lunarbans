<pre>
<?php
$db = new Database();

$banned = 0;
$sql = "SELECT * FROM lb_databases";
$gotBanned = 0;
$dbnames = $db->query($sql);
$info = array();
$steamid_chars = '/^STEAM_/';
foreach ($dbnames as $key => $value) {
    $dbname = $value['amxbans'];
    $sql = ("SELECT * FROM $dbname.amx_serverinfo");
    $serverinfo = $db->query($sql);
    foreach ($serverinfo as $val) {
        $hostaddr = explode(":", $val['address']);
        $ip = $hostaddr[0];
        $port = $hostaddr[1];
        $rcon = $val['rcon'];
        $info[$dbname]['info']['host'] = $val['hostname'];
        $info[$dbname]['info']['address'] = $val['address'];
        $info[$dbname]['info']['ip'] = $ip;
        $info[$dbname]['info']['port'] = $port;
        if($ip && $port) {
            $cs = new Rcon();
                $cs->Connect($ip, $port, $rcon);
                $csinfo = $cs->ServerInfo();
                if ($csinfo) {
                    $info[$dbname]['info']['map'] = $csinfo['map'];
                    $info[$dbname]['info']['maxplayers'] =  $csinfo['maxplayers'];
                    $info[$dbname]['info']['activeplayers'] = $csinfo['activeplayers'];
                    if ($csinfo['activeplayers'] != "0") {
                        for ($i=1; $i <= $csinfo['activeplayers']; $i++) {
                            $info[$dbname]['info']['players'][$i] = $csinfo[$i];
                        }

                    }
                }
                $cs->Disconnect();
        }



    }

    $sql = ("SELECT * FROM $dbname.amx_amxadmins");
    $admins = $db->query($sql);
        $info[$dbname]['info']['totaladmins'] = @sizeof($admins);
    if (is_array($admins)) {
        foreach ($admins as $val) {
            $username = $val['username'];
            if(preg_match($steamid_chars, $val['steamid'])) {
                $steamid = $val['steamid'];
                $sql = "SELECT * FROM $dbname.amx_bans WHERE admin_id = '$steamid'";
                $banned = $db->queryCount($sql);
                $sql = "SELECT * FROM $dbname.amx_bans WHERE player_id = '$steamid'";
                $gotBanned += $db->queryCount($sql);
            }
            else {
                $sql = "SELECT * FROM $dbname.amx_bans WHERE admin_nick = '$username'";
                $banned = $db->queryCount($sql);
                $sql = "SELECT * FROM $dbname.amx_bans WHERE player_nick = '$steamid'";
                $gotBanned = $db->queryCount($sql);
            }

            $info[$dbname]['adminlist'][$val['id']]['username'] = $val['username'];
            $info[$dbname]['adminlist'][$val['id']]['steamid'] = $val['steamid'];
            $info[$dbname]['adminlist'][$val['id']]['created'] = $val['created'];
            $info[$dbname]['adminlist'][$val['id']]['expired'] = $val['expired'];
            $info[$dbname]['adminlist'][$val['id']]['days'] = $val['days'];
            $info[$dbname]['adminlist'][$val['id']]['flags'] = $val['flags'];
            $info[$dbname]['adminlist'][$val['id']]['access'] = $val['access'];
            $info[$dbname]['adminlist'][$val['id']]['banned'] = $banned;
            $info[$dbname]['adminlist'][$val['id']]['gotBanned'] = $gotBanned;
        }
    }

    $sql = "SELECT * FROM $dbname.amx_bans";
    $bans = $db->query($sql);
    $info[$dbname]['info']['totalbans'] = @sizeof($bans);
    if (is_array($bans)) {
        foreach ($bans as $val) {
            $nick = $val['player_nick'];
            $sql = "SELECT * FROM $dbname.amx_bans where player_nick = '$nick'";
            $bannedbefore = $db->queryCount($sql);
            $bannedbefore = $bannedbefore -1;
            $info[$dbname]['banlist'][$val['bid']]['username'] = $val['player_nick'];
            $info[$dbname]['banlist'][$val['bid']]['player_ip'] = $val['player_ip'];
            $info[$dbname]['banlist'][$val['bid']]['player_id'] = $val['player_id'];
            $info[$dbname]['banlist'][$val['bid']]['created'] = $val['ban_created'];
            $info[$dbname]['banlist'][$val['bid']]['reason'] = $val['ban_reason'];
            $info[$dbname]['banlist'][$val['bid']]['type'] = $val['ban_type'];
            $info[$dbname]['banlist'][$val['bid']]['admin'] = $val['admin_nick'];
            $info[$dbname]['banlist'][$val['bid']]['length'] = $val['ban_length'];
            $info[$dbname]['banlist'][$val['bid']]['server_name'] = $val['server_name'];
            $info[$dbname]['banlist'][$val['bid']]['bannedbefore'] = $bannedbefore;

            $expired =  date('Y-m-d H:i', $val['ban_created']);
            $time = time();
            $date = date("Y-m-d H:i",$time);
            //echo "$date";
            $today_time = strtotime($date);
            $expire_time = strtotime($expired);
            if ($today_time > $expire_time) {
                $info[$dbname]['banlist'][$val['bid']]['expired'] = "1";
            } elseif($val['ban_length'] == "0") {
                $info[$dbname]['banlist'][$val['bid']]['expired'] = "0";
            }
            else {
                $info[$dbname]['banlist'][$val['bid']]['expired'] = "0";
            }
            //echo date('Y-m-d H:i',strtotime('+'$val['ban_length']' minutes',strtotime($start)));
        }
    }
}
print_r($info);
 ?>
